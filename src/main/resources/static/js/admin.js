
var admin = function(p_notificationPanel, p_tableEmail, p_tableIngridients, p_tableCashFlow, p_toastElement, p_soundPlayer) {
	
	var panel = p_notificationPanel;
	var toast = p_toastElement;
	var tableEmail = p_tableEmail;
	var tableIngridients = p_tableIngridients
	var tableCashFlow = p_tableCashFlow
	var soundPlayer = p_soundPlayer;
	var orderCounter = parseInt(0);
	
	var result=new Array();
	
	function validateNumber  (event) {
		var key = window.event ? event.keyCode : event.which;
		if (event.keyCode === 8 || event.keyCode === 46) {
			return true;
		} else if (key < 48 || key > 57) {
			return false;
		} else {
			return true;
		}
	};
	
	/**
	 * JQUERY KOMPONENTA ZA GENERIRANJE <strong> captiona u html-u
	 */
	var caption = function (title){
    	return $('<strong/>',{
	    	html: title
	    }).append('<br>');
	}
    
    function createTableEmails(data, totaler){
    	var $table_email = $('<table/>').addClass('table table-hover').attr("id","emails");//.appendTo(dayliTotal);
 	    createHeaderTable(new Array('Name','Surname','Email','Sum'),'unique-color').appendTo($table_email);
 	   
 		$.each(data.consumers, function(index, value){
             var all_data = new Array(value.firstName , value.surName , value.email, value.quantity);
             totaler.add(value.quantity);
             $table_email.append(createRow(all_data));
 		});
 		console.log($table_email.html());
 		return $table_email;
    }
    
    function createTableIngridients(data, totaler){
    	var $table_ingridients = $('<table/>').addClass('table table-hover');
    	
		createHeaderTable(new Array('Ingridient','Quantity(grams)','Price(per kg)','Sum'),'unique-color').appendTo($table_ingridients);
			$.each(data.ingridients, function(index, value){
				var sum =Math.round((value.quantity*value.price)/1000);
				totaler.add(sum);
				var data_ = new Array(value.ingridientTypeValue,value.quantity ,value.price, sum);
				$table_ingridients.append(createRow(data_));
			});
		return $table_ingridients;
    }
    
    function createTableTotal(data){
    	var $table_total = $('<table/>').addClass('table table-hover');
		createHeaderTable(new Array('Desc','Total earned','Total spent','Earned'),'danger-color').appendTo($table_total);
		var data = new Array(data.menuDescription, Math.round(data.totalEarned),Math.round(data.totalSpent), (Math.round(data.totalEarned) - Math.round(data.totalSpent)));
		$table_total.append(createRow(data));
		return $table_total;
    }
    
    var totalRow = function(){
    	var x = 0;
    	return {
    		add: function(val){
        		x += parseInt(val);
        	},
        	render:function(colspan, text){
        		var table_row = $('<tr/>');
        		($('<td/>')).attr('colspan',colspan).text(text).appendTo(table_row);
        		($('<td/>')).text(x).appendTo(table_row);
        		return table_row;
        	},
        	get: function(){
        		return x;
        	},
        	reset:function(){
        		x=0;
        	}
    	}
    }
   
    var createRow = function(data){
    	var table_row = $('<tr/>');
    	$.each(data, function(index,value){
    		table_row.append($('<td/>').text(value));
    	});
	  return table_row;
    } 
    
    function createHeaderTable(data, headerClass){
    	var header =$('<thead/>',{class: headerClass })
		var $theadRow = $('<tr/>');
    	$.each(data, function(index, value){
    		  $theadRow.append($('<th/>').attr('scope','col').text(value))
    	});
    	$theadRow.appendTo(header);
    	return header;
    }
	
    /** 
     * STATISTIKA PRODAJE PO DANU
     */
	function showDailyStats(data){
		
		caption(data.wholeDate).appendTo(tableEmail);
		caption('Konzumenti').appendTo(tableEmail);
		var emailTotal = totalRow();
		createTableEmails(data, emailTotal).appendTo(tableEmail);
		var $totalEmail =  $('<div/>').html("Ukupno naruđbi: "+ emailTotal.get());
		$totalEmail.appendTo(tableEmail)
		
		var ingridentsTotal = totalRow();
		tableIngridients.append(caption('Namirnice'));
		createTableIngridients(data,ingridentsTotal).appendTo(tableIngridients)
		var $totalIngridients =  $('<div/>').html("Ukupno namirnice: "+ ingridentsTotal.get());
		$totalIngridients.appendTo(tableIngridients)
		
		tableCashFlow.append(caption('Total:'));
		createTableTotal(data).appendTo(tableCashFlow)
	};
	
	/**
	 * VALIDIRA I SUMIRA REDAK U TABLICI PRILIKO DODAVANJE NOVE NAMIRNICE U MENI
	 */
	function validateAndSumRow(row){
		var sum = 1;
		console.log($( "input[name$='quantity']", row ).val());
		console.log(row.html() + $( "input[name$='quantity']", row ).val().length);
		console.log(row.html() + $( "input[name$='price']", row ).val().length);
		if($( "input[name$='quantity']", row ).val().length>=1 && 
				$( "input[name$='price']", row ).val().length>=1 &&
				$( "input[name$='ingridientTypeValue']", row ).val().length>1) {
			sum=Math.round($( "input[name$='price']", row ).val()* $( "input[name$='quantity']", row ).val());
			// kolicina se upisuje u gramima
			$( "input[name$='sum']", row ).val(Math.round(sum/1000))
			return true;
		}
		alert('Popunite kolicinu i jedinicnu cijenu!')
		return false;
	}
	
	/**
	 * DODAJE REDAK PRILIKOM ZADAVANJA NOVOG MENIJA.. ODNOSNO DODAJE NAMIRNICE U MENI
	 */
	
	function addRow(container, index, sum){
		var $row;
		if(index !== 0){
			$row = $('.newIngridient', container).last().clone();
			if(sum && !validateAndSumRow($('.newIngridient', container).last())){
				return false;
			}
		}else{
			$row = $('.newIngridient', container).last();
		}
//		console.log($row.html());
		// nova namirnica inicijalizacija
		$( "input[name$='ingridientType']", $row ).val('');
		$( "input[name$='quantity']", $row ).val('');
		$( "input[name$='quantity']", $row ).on({
			keypress : validateNumber
		});
		$( "input[name$='price']", $row ).val('');
		$( "input[name$='price']", $row ).on({
			keypress : validateNumber
		});
		
		$( "input[name$='sum']", $row ).val('');
		$( "input[name$='sum']", $row ).attr("disabled", true)
		
		$( "input[name$='ingridientTypeValue']", $row ).val('');
		$( "input[name$='ingridientTypeValue']", $row ).autocomplete({
			
									      source: result,
									      focus: function( event, ui ) {
									    	  $( "input[name$='ingridientTypeValue']", $row ).val( ui.item.value );
									          return false;
									        },
									        
									      select: function( event, ui ) {
									    	  console.log(ui.item.id);
									    	  $( "input[name$='ingridientTypeValue']", $row ).val( ui.item.value);
							                  $("input[type='hidden']",$row).val(''+ui.item.id);
							                  // focus quantnty
							                  $( "input[name$='quantity']", $row ).focus();
									          return false;
									        }
		 });
		
		// preimenovanje polja zbog bindinga za kontroler
		$('input', $row).each(function(){
			var $i = $(this)
			//  ingridient[0].ingridientType => ingridientType
			var name = $i.attr('name').match('[^.]*$').toString();
			var new_name = "ingridients["+index+"]."+name;
			$i.attr("name", new_name);
			$i.attr("id", new_name);
		});
		
		if(index !== 0 ){
			console.log($row.html());
			console.log(container.html());
			container.append($row);
			console.log('after-->'+container.html());
			var in_id = "ingridients["+index+"].ingridientTypeValue";
			$("[id='"+in_id+"']",container).focus();
		}
	}
	
	/**
	 * CREATES ONE  ORDER DIV
	 */
	function createOrderDiv(index, value){
		var notification_panel = $("#order_panel")
		var orderNumber = index + 1;
		var $order = $("<div/>",{class: "order" });
		$order.attr('style','cursor: move; postion: relative')
		var $header= $("#order_header",notification_panel).clone();
		$header.text("Narudžba broj:"+orderNumber );
		$header.attr('style', 'font-family: Arial, Helvetica, sans-serif; font-weight:1200; background-color:infobackground;')
		var $orderHeader = $("#order_header1", notification_panel).clone();
		$orderHeader.text(value.email );
		$orderHeader.attr('style', 'font-family: Arial, Helvetica, sans-serif; font-weight:900; background-color:infobackground;')
		var $row1 = $("#order_row", notification_panel).last().clone();
		$("div:first-child",$row1).text('Poruka');
		$("div:last-child",$row1).text(value.message);
		var $row2 = $("#order_row", notification_panel).last().clone();
		$("div:first-child",$row2).text('Adresa');
		$("div:last-child",$row2).text(value.adress);
		var $row3 = $("#order_row", notification_panel).last().clone();
		$("div:first-child",$row3).text('Mobitel');
		$("div:last-child",$row3).text(value.cellular);
		var $row4 = $("#order_row", notification_panel).last().clone();
		$("div:first-child",$row4).text('Kolicina');
		$("div:last-child",$row4).text(value.quantity);
		
		$order.append($header);
		$order.append($orderHeader);
		$order.append($row1);
		$order.append($row2);
		$order.append($row3);
		$order.append($row4);
		$order.append("<hr>");
		console.log($order.html())
		return $order;
	}
	
	/**
	 * DODAJE NOVU NARUDZBU NA ADMIN PANEL
	 */
	function addNewOrders(data){
		if(data === undefined){
			$("#order_panel", panel).attr('style','display:block')
			return;
		}
		$("#order_panel", panel).attr('style','display:none')
		var notification_panel = $("#order_panel")
		$.each(data,function(index, value){
			var $order = createOrderDiv(index, value)
			panel.append($order);
			$order.draggable();
		});
	}
	
	/**
	 * INICIJALIZIRA SVE NAMIRNICE IZ BAZE U DINAMICKE REDOVE S AUTOCOMPLETEOM
	 */
	function initializeExistingIngridients($container){
		$('div', $container).each(function(){
			var $a = $(this)
			$('input', $a).each(function(){
				var $i = $(this)
				var name = $i.attr('name').match('[^.]*$').toString();
				var nameAll = $i.attr('name');
				if(name ==='ingridientTypeValue'){
					$i.autocomplete({
					      source: result,
					      focus: function( event, ui ) {
					    	  $( "input[name$='ingridientTypeValue']", $a ).val( ui.item.value );
					          return false;
					      },
					      select: function( event, ui ) {
					    	  console.log(ui.item.id);
					    	  $( "input[name$='ingridientTypeValue']", $a ).val( ui.item.value);
			                  $("input[type='hidden']",$a).val(''+ui.item.id);
			                  // focus quantnty
			                  $( "input[name$='quantity']", $a ).focus();
					          return false;
					        }
					});
				}else if(name ==='price'){
					$i.on({
						keypress : validateNumber
					});
				}else if(name ==='quantity'){
					$i.on({
						keypress : validateNumber
					});
					
				}else if(name === 'sum'){
					$i.attr("disabled", true)
				}
				
				
			});
			
		});
		var sumIngridients = function(){
			var shouldAdd = false;
			return{
				setAddFlag:function(){
					shouldAdd=true;
				},
				get:function(){
					return shouldAdd;
				}
			}
			
		};
		
		//####################################################################
		var add = sumIngridients();
		var $ingridientsGenerated = $('#ingridientsGenerated');
		console.log('container removeRowUpdate-->'+$ingridientsGenerated.html());
		$('#addRowUpdate').click(
				function(e){
					e.preventDefault();
					var index = $(".newIngridient",$ingridientsGenerated).length;
					addRow($ingridientsGenerated,index, add.get());
					add.setAddFlag();
					
				}
		);
		
		//####################################################################
		var $ingridientsGenerated1 = $('#ingridientsGenerated');
		console.log('container removeRowUpdate-->'+$ingridientsGenerated1.html());
		$('#removeRowUpdate').click(
				function(e){
					var index = $(".newIngridient",$ingridientsGenerated1).length;
					var $last_row = $('.newIngridient', $ingridientsGenerated1).last();
					if(index>1){
						$($last_row).remove();
					}
				});
	}
	/**
	 * INICIJALIZIRA PRVU PRAZNU NAMIRNICU S AUTOCOMPLETEOM
	 */
	function initializeFirstRow($container){
		addRow($container,0, false);
		console.log('container addRow-->'+$container.html());
		$('#addRow').click(
				function(e){
					e.preventDefault();
					var index = $(".newIngridient",$container).length;
					addRow($container,index, true);
				}
		);
		var $container1 = $('#container');
		console.log('container removeRow-->'+$container1.html());
		$('#removeRow').click(
				function(e){
					var index = $(".newIngridient",$container1).length;
					var $last_row = $('.newIngridient', $container1).last();
					if(index>1)
						$($last_row).remove();
				});

	}
	/**############################################
	 * 
	 * ajax calls to back end
	 * 
	 * ############################################
	 */
	
	
	return {
		init : function() {
			var i = 0;
			$('.toast').toast('show');
			var $containerAll =$('#ingridientsGenerated');
			var $containerFirst = $('#container');
			if($containerAll === 'undefined' || $containerAll.length === 0){
				initializeFirstRow($containerFirst);
			}else{
				initializeExistingIngridients($containerAll);
			}
			
		},
		getDailyMenus : function() {

			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='X-CSRF-TOKEN']").attr("content");

			var currentDate = new Date().toISOString().slice(0, 10);
			$.ajax({
				type : "GET",
				beforeSend : function(request) {
					request.setRequestHeader(header, token);
				},
				url : '/admin/user/orders',
				dataType : 'json',
				type : 'GET',
				contentType : 'application/json',
				processData : false,
				success : function(data, textStatus, jQxhr) {
					panel.empty();
					addNewOrders(data);
					var orderNum = parseInt(data !=='undefined'?data.length:0);
					if(orderCounter < orderNum){
						orderCounter = parseInt(orderNum);
						soundPlayer.play('cash');
					}else if(orderCounter>orderNum){
						orderCounter = parseInt(orderNum);
						soundPlayer.play('ouuch');
					}
				},
				error : function(jqXhr, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});

		},
		getDailyStatistic : function(date) {
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='X-CSRF-TOKEN']").attr("content");

			var currentDate = new Date().toISOString().slice(0, 10);
			$.ajax({
				type : "GET",
				beforeSend : function(request) {
					request.setRequestHeader(header, token);
				},
				//2011-01-01
				url : '/admin/statistic?date='+date,
				dataType : 'json',
				contentType : 'application/json',
				success : function(data, textStatus, jQxhr) {
					tableCashFlow.empty();
					tableEmail.empty();
					tableIngridients.empty();
					showDailyStats(data);
				},
				error : function(jqXhr, textStatus, errorThrown) {
					tableCashFlow.empty();
					tableEmail.empty();
					tableIngridients.empty();
					console.log(errorThrown);
				}
			});

		},
		getDailyMenu : function(form) {
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='X-CSRF-TOKEN']").attr("content");

			var currentDate = new Date().toISOString().slice(0, 10);
			$.ajax({
				type : "GET",
				beforeSend : function(request) {
					request.setRequestHeader(header, token);
				},
				//2011-01-01
				url : '/admin/menu',
				dataType : 'json',
				contentType : 'application/json',
				success : function(data, textStatus, jQxhr) {
					 $('#menuId',form).val(data.id);
					 $('#available',form).val(data.available);
					 console.log(data);
				},
				error : function(jqXhr, textStatus, errorThrown) {
		
					console.log(errorThrown);
				}
			});

		},
		getAllIngridients : function() {

			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='X-CSRF-TOKEN']").attr("content");

			$.ajax({
				type : "GET",
				beforeSend : function(request) {
					request.setRequestHeader(header, token);
				},
				url : '/admin/ingridients',
				dataType : 'json',
				contentType : 'application/json',
				success : function(data, textStatus, jQxhr) {
					$.each(data, function(key, value) {
//					    console.log(key+ "-->"+value.id + " " +value.type + " " +value.carbon);
					    var obj = {
					    		id: value.id, 
					    		value:value.type,
					    		fat: value.fat,
					    		carbon: value.carbon,
					    		protein: value.protein,
					    		calories: value.calories
					    };
					    result.push(obj);
					});
				},
				error : function(jqXhr, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});

		}
	}
};