var sound = function() {


	var sounds = {
		"cash" : {
			url : "/sound/cash_register_x.wav",
			volume : 3
		},
		"ouuch" : {
			url : "/sound/aah-01.wav",
			volume : 3
		}
	};

	var soundContext = new AudioContext();

	function loadSound(name) {
		var sound = sounds[name];
		var url = sound.url;
		var buffer = sound.buffer;
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.responseType = 'arraybuffer';
		request.onload = function() {
			soundContext.decodeAudioData(request.response, function(newBuffer) {
				sound.buffer = newBuffer;
			});
		}
		request.send();

	}

	return {
		init : function() {
			for ( var key in sounds) {
				loadSound(key);
			}
		},
		play : function(name) {
			console.log(name)
			var sound_to_play = sounds[name];
			var soundVolume = sounds[name].volume || 1;

			var buffer = sounds[name].buffer;
			if (buffer) {
				var source = soundContext.createBufferSource();
				source.buffer = buffer;

				var volume = soundContext.createGain();
				
				volume.gain.value = soundVolume;
				volume.connect(soundContext.destination);
				source.connect(volume);
				source.start(0);

			}
		}
	}
}
