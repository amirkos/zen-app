package hr.optimus.zen.config;

public class ZenAppUrlConstants {
	
	public static final String REDIRECT_USER = "redirect:/user";

	public static final String REDIRECT_ADMIN = "redirect:/admin";
	
	public static final String REDIRECT_INDEX = "redirect:/";
	
	public static final String REDIRECT_LOGIN = "redirect:/login";

	public static final String INDEX = "sneaky/index";
	
	public static final String USER_INDEX = "user/index";
	
	public static final String ADMIN_INDEX = "admin/index";
	
	public static final String REGISTER = "sneaky/register";
	
	public static final String ERROR = "error";
	public static final String ADMIN ="ADMIN";

}
