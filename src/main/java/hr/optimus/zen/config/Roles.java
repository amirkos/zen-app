package hr.optimus.zen.config;

public enum Roles {
	
	ROLE_ADMIN,
	ROLE_USER,
	ROLE_FACEBOOK,
	ROLE_GOOGLE

}
