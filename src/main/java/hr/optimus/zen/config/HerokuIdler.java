package hr.optimus.zen.config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HerokuIdler {

	@Scheduled(cron = "0 */10 6-12 * * MON-FRI")
	public void herokuNotIdle() {
		log.debug("Heroku not idle execution");
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject("https://zen-k.herokuapp.com/", Object.class);
	}
}
