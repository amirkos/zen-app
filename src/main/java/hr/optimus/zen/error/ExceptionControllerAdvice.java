package hr.optimus.zen.error;


import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
 
 
@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class ExceptionControllerAdvice {
	
	private final HttpServletRequest request;
 
    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception ex) {
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    UserBean user = auth != null ? (UserBean) auth.getDetails() : new UserBean();
	    String error1 = "IP:"+ getClientIp()+ ' '+user.getEmail() + ' ' + ex.getMessage();
	    log.error(error1, ex);
        ModelAndView model = new ModelAndView();
        model.addObject("errMsg", "This is a 'Exception.class' message.");
        model.setViewName("error");
        return model;
 
    }
    
    
    private  String getClientIp() {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }
}