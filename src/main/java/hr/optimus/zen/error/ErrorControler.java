package hr.optimus.zen.error;

import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.config.ZenAppConfig;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller 
@Slf4j
@RequiredArgsConstructor
public class ErrorControler implements ErrorController { 
	
    private final ErrorAttributes errorAttributes;
    
    private final ZenAppConfig config;
    
    private final EmailService emailService;
    
	
	@RequestMapping("/error")
	public ModelAndView handleError(HttpServletRequest request, WebRequest webRequest) {
	    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
	    Integer statusCode = null;
	    String redirect = "";
	    String error = null;
	    if (status != null) {
	        statusCode = Integer.valueOf(status.toString());
	        String message = (String)request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
	        
	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	        	error =  "error-404 -"+ message;
	        	redirect = ZenAppUrlConstants.REDIRECT_INDEX;
	        }
	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	        	error = "error-500 -" + message;
	        	redirect = ZenAppUrlConstants.ERROR;
	        	
	        }else if(statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
	        	error = "error-405 -"+ message;
	        	redirect = ZenAppUrlConstants.ERROR;
	        }
	        
	        if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value() || statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
		    	StringBuffer retBuf = generateErrorMessage(webRequest, statusCode, message);
	 	        emailService.sendErrorMail(config.getErrorEmail(), error,retBuf.toString());
		    }
		   
	    }
	    
	    ModelAndView mav = new ModelAndView(redirect);
	    mav.addObject("error", error);
	    return mav;
	}

	

	private StringBuffer generateErrorMessage(WebRequest webRequest, Integer statusCode, String message) {
		Map<String, Object> body = errorAttributes.getErrorAttributes(webRequest, true);
		// Extract stack trace string.
		String trace = (String) body.get("trace");

		StringBuffer retBuf = new StringBuffer();
		retBuf.append("<pre>");

		if(statusCode != null)
		{
		    retBuf.append("Status Code : ");
		    retBuf.append(statusCode);
		}

		if(message != null && message.trim().length() > 0)
		{
		    retBuf.append("\n\rError Message : ");
		    retBuf.append(message);
		}

		if(trace != null){
		    retBuf.append("\n\rStack Trace : ");
		    retBuf.append(trace);
		}
		
		log.error(retBuf.toString());
		return retBuf;
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return ZenAppUrlConstants.INDEX;
	}

}
