package hr.optimus.zen.service.impl;

import org.springframework.stereotype.Service;

import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepositoryCustom userRepositry;

	@Override
	public UserBean findByEmail(String email) {
		return userRepositry.findByEmail(email);
	}

	@Override
	public void deleteByEmail(String email) {
		userRepositry.deleteUserByEmail(email);
	}

	@Override
	public UserBean save(UserBean user) {
		return userRepositry.save(user);
	}

}
