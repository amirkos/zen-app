package hr.optimus.zen.service.impl;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import hr.optimus.zen.config.ZenAppConfig;
import hr.optimus.zen.mail.Mail;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
	private final JavaMailSender emailSender;
	private final ZenAppConfig config;
	private final Environment env;
	private final BCryptPasswordEncoder encoder;
	private final SpringTemplateEngine templateEngine;

	public static final String DailyMenuEmailTemplate = "mailtemplates/menu-mail-template";
	public static final String OrderConfirmationEmailTemplate = "mailtemplates/order-confirmation-mail-template";
	public static final String OrderCancleationEmailTemplate = "mailtemplates/order-cancel-mail-template";
	public static final String registrationConfirmationEmailTemplate = "mailtemplates/registration-confirmation-mail-template";

	@Async
	public void sendSimpleMessage(Mail mail, String template) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());

			Context context = new Context(new Locale("hr","HR"));
			context.setVariables(mail.getModel());
			String html = templateEngine.process(template, context);
			helper.setFrom(mail.getFrom());
			helper.setTo(mail.getTo());
			helper.setText(html, true);
			helper.setSubject(mail.getSubject());
			helper.setFrom(mail.getFrom());

			emailSender.send(message);
		} catch (Exception e) {
			System.out.println("neki error se desio" + e.toString());
			e.printStackTrace();
		}

	}
	
	public String TestsendSimpleMessage(Mail mail, String template) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			
			Context context = new Context(LocaleContextHolder.getLocale());
			context.setVariables(mail.getModel());
		
			String html = templateEngine.process(template, context);
			String inlineImage = "<img src=\"cid:logo.png\"></img><br/>";
			helper.setText(inlineImage + html, true);
			Resource res = new ClassPathResource("static/img/logo.png");
			System.out.println("ClassPathResource-->"+res.getURI() + " " + res.getFilename() + "locale:"+(LocaleContextHolder.getLocale()));
			helper.addInline("logo", res,"image/png");
			
			return html;
		} catch (Exception e) {
			System.out.println("neki error se desio" + e.toString());
			e.printStackTrace();
		}
		return "KITA";
	}
	
	

	@Async
	public void sendRegistrationConfirmationEmail(UserBean user) {

		Mail mail = new Mail();
		mail.setFrom(this.env.getProperty("spring.mail.username"));
		mail.setTo(user.getEmail());
		mail.setSubject("Zen-k ~ Potvrda registracije");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user.getFirstName());
		model.put("mail", config.getWebMail());
		model.put("cellular",config.getWebCellullar());
		model.put("baseUrl", config.getWebAdres());
		model.put("registration", getRegistrationConfirmation(user));
		mail.setModel(model);

		sendSimpleMessage(mail, registrationConfirmationEmailTemplate);

	}

	private String generateUnsubscribeLink(UserBean user) {
		return "/unsubscirbe?email=" + user.getEmail() + "&token="
				+ encoder.encode(user.getEmail() + user.getProvider());
	}

	private String getRegistrationConfirmation(UserBean user) {
		String url = "/registration/confirmation?email=" + user.getEmail() + "&token="
				+ encoder.encode(user.getEmail() + user.getProvider());
		return url;
	}

	@Async
	public void sendCancleOrderEmail(UserBean user, UserOrder order) {
		Mail mail = new Mail();
		mail.setFrom(this.env.getProperty("spring.mail.username"));
		mail.setTo(user.getEmail());
		mail.setSubject("Zen-k ~ otkazivanje narudžbe");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user.getFirstName());
		model.put("order", order);
		model.put("baseUrl", config.getWebAdres());
		model.put("unsubscribe", generateUnsubscribeLink(user));
		model.put("web", this.config.getWebAdres());
		model.put("cellular", this.config.getWebCellullar());
		model.put("mail", this.env.getProperty("spring.mail.username"));
		model.put("logo", "logo.png");
		mail.setModel(model);
		sendSimpleMessage(mail, OrderCancleationEmailTemplate);
	}

	@Async
	public void sendOrderConfirmationEmail(UserOrder order) {
		Mail mail = new Mail();
		mail.setFrom(this.env.getProperty("spring.mail.username"));
		mail.setTo(order.getUser().getEmail());
		mail.setSubject("Zahvaljujem na narudžbi");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", order.getUser().getFirstName());
		model.put("order", order);
		model.put("day", LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE dd.MMMM.yyyy", new Locale("hr","HR"))));
		model.put("web", this.config.getWebAdres());
		model.put("cellular", this.config.getWebCellullar());
		model.put("mail", this.env.getProperty("spring.mail.username"));
		model.put("unsubscribe", generateUnsubscribeLink(order.getUser()));
		model.put("baseUrl", config.getWebAdres());
		model.put("logo", "logo");
		mail.setModel(model);
		sendSimpleMessage(mail, OrderConfirmationEmailTemplate);
	}

	@Async
	public void sendMenuEmail(UserBean user, Menu menu) {
		Mail mail = new Mail();
		mail.setFrom(this.env.getProperty("spring.mail.username"));
		mail.setTo(user.getEmail());
		String day = LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE dd.MM.yyyy", new Locale("hr","HR")));
		mail.setSubject("Meni za " + day);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user.getFirstName());
		model.put("menu", menu);
		model.put("day", day);
		model.put("web", this.config.getWebAdres());
		model.put("cellular", this.config.getWebCellullar());
		model.put("mail", this.env.getProperty("spring.mail.username"));
		model.put("unsubscribe", generateUnsubscribeLink(user));
		model.put("baseUrl", config.getWebAdres());
		model.put("logo", "logo");
		mail.setModel(model);
		sendSimpleMessage(mail, DailyMenuEmailTemplate);
	}

	@Async
	public void sendErrorMail(String to,String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(to); 
        message.setSubject("ZEN-K-ERROR - "+ subject); 
        message.setText(text);
        emailSender.send(message);
		
	}



}
