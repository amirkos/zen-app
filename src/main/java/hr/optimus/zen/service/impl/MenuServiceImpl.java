package hr.optimus.zen.service.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.impl.UserOrderRepositoryCustomImpl;
import hr.optimus.zen.repository.impl.UserRepositoryCustomImpl;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.service.MenuService;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.ConsumersDTO;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.mapper.IngridientMapper;
import hr.optimus.zen.web.model.dto.mapper.MenuMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class MenuServiceImpl implements MenuService {
	
	private final MenuRepository menuRepository;
	private final UserOrderRepositoryCustomImpl userOrder;
	private final UserRepositoryCustomImpl userRepository;
	private final EmailService service;
	@Qualifier("IngridientMapperImp") @NonNull @Lazy private final IngridientMapper IngridientMapperImp;
	@Qualifier("MenuMapperImp")  @NonNull private final MenuMapper MenuMapperImp;

	private final BigDecimal THOUSAND_GRAMS = new BigDecimal(1000);
    private final BigDecimal DELIVERY_BOY = new BigDecimal(100);
    private final BigDecimal FOOD_BOX = BigDecimal.ONE;

	
	

	public MenuDTO insertMenu(MenuDTO dto) {
		boolean sendEmail = dto.getSendEmail();
		Date today = new Date(System.currentTimeMillis());
		Menu menu = MenuMapperImp.toEntity(dto);
		Menu  dailyMenu = menuRepository.getDailyMenu(today);
		
		if(dailyMenu == null) {
			menu.setTotalAvailable(menu.getAvailable());
			menu.setAvailable(menu.getTotalAvailable());
			menu.setRecordTime(new Timestamp(System.currentTimeMillis()));
			dailyMenu = menuRepository.save(menu);
		}
		else {
			// update daily menu
			dailyMenu.setMenuDescription(menu.getMenuDescription());
			dailyMenu.setMenuFacts(menu.getMenuFacts());
		
			/**
			 * SLUCAJ KAD SE AZURIRA BROJ PREOSTALIH OBROKA
			 * NE SMIJE BITI MANJI OD BROJA PRESTALIH OBROKA
			 * 
			 */
			Integer tmp = dailyMenu.getAvailable().intValue() + menu.getAvailable();
			
			// ako je ostalo 0 ili vise obroka tek onda azuriraj
			if(tmp>=0 && dailyMenu.getAvailable().intValue() != menu.getAvailable().intValue()) {
				
				log.info("Setting new available for :" + dailyMenu.getMenuDate() + 
						" old value: dailyMenu available:" + dailyMenu.getAvailable() + 
						" total_available:" +dailyMenu.getTotalAvailable() );
				
				dailyMenu.setAvailable(tmp);
				
				dailyMenu.setTotalAvailable(dailyMenu.getTotalAvailable() + menu.getAvailable());
				
				log.info("Setting new available for :" + dailyMenu.getMenuDate() + 
						" new value: dailyMenu available:" + dailyMenu.getAvailable() + 
						" total_available:" +dailyMenu.getTotalAvailable() );
				
			}
			
			if(menu.getIngridients().size()>0) {
				// first clear than add all
				dailyMenu.getIngridients().clear();
				dailyMenu.getIngridients().addAll(menu.getIngridients());
			}
			
			dailyMenu.setPrice(menu.getPrice());
			dailyMenu.setRecordTime(new Timestamp(System.currentTimeMillis()));
			dailyMenu = menuRepository.save(dailyMenu);
		}
		if(sendEmail) {
			for(UserBean u: userRepository.findAllUsers()) {
				service.sendMenuEmail(u, dailyMenu);
			}
		}
		MenuDTO ret = MenuMapperImp.toDTO(dailyMenu);
		return ret;

	}

	
	public Menu deleteMenu(Menu menu) {
		menuRepository.delete(menu);
		return menu;
	}


	public List<Menu> getMenus(Date from, Date until) {
		List<Menu> menus = menuRepository.findAll();
		return (List<Menu>) menus.stream().filter(f-> {
			return f.getMenuDate().after(from) && f.getMenuDate().before(until);
			}).collect(Collectors.toList());	
		
	}


	public MenuCalculationDTO getDailyTotal(Date date)  throws Exception {
		List<UserOrder> orders = userOrder.getDailyOrders(date);
		if(orders == null) {
			// no food 4 U
			return null;
		}
		Menu menu = orders.get(0).getMenu();
		double order_total = orders.stream().mapToDouble(mapper -> mapper.getQuantity()).sum();// menu.getTotalAvailable().intValue() - menu.getAvailable();
		double total_earned = order_total*menu.getPrice();//(menu.getTotalAvailable().intValue() - menu.getAvailable())* menu.getPrice();
		
		double ingridients_sum = menu.getIngridients()
				.stream()
				.mapToDouble(f-> f.getPrice().doubleValue() * (f.getQuantity().doubleValue()/THOUSAND_GRAMS.doubleValue()))
				.sum();
		MenuCalculationDTO calculation = new MenuCalculationDTO();

		// samo emailovi
		calculation.setConsumers(orders.stream().map(m->{
			ConsumersDTO dto = new ConsumersDTO();
			dto.setMessage(m.getMessage());
			dto.setFirstName(m.getUser().getFirstName());
			dto.setSurName(m.getUser().getLastName());
			dto.setQuantity(m.getQuantity().toString());
			dto.setEmail(m.getUser().getEmail());
			
			return dto;
			}).collect(Collectors.toList()));		
		
		calculation.setIngridients(menu.getIngridients().stream()
				.map(mapper->IngridientMapperImp.toDto(mapper)
		).collect(Collectors.toList()));
		
		calculation.setMenuDate(menu.getMenuDate());
		calculation.setMenuDescription(menu.getMenuDescription());
		calculation.setLocaleDate(menu.getMenuDate());
		calculation.setTotalSpent(new BigDecimal(ingridients_sum)
				.add(DELIVERY_BOY) // dostavljac 
				.add(FOOD_BOX.multiply(new BigDecimal(order_total)))); // posude
		calculation.setTotalEarned(new BigDecimal(total_earned));
		
		return calculation;
	}

	/**
	 * get daily total for period
	 */

	public List<MenuCalculationDTO> getTotalBetweenDates(Date dateFrom, Date dateUntil) throws Exception {
		 Calendar startDate = Calendar.getInstance();
		 startDate.setTime(dateFrom);
		 Calendar endDate = Calendar.getInstance();endDate.setTime(dateUntil);
		 endDate.setTime(dateUntil);
		 List<MenuCalculationDTO> calculations = new ArrayList<MenuCalculationDTO>();
		 while(startDate.before(endDate)) {
			 calculations.add(getDailyTotal(new Date(startDate.getTimeInMillis())));
			 startDate.add(Calendar.DATE, 1);
		 }
		// TODO Auto-generated method stub
		return calculations;
	}


	@Override
	public MenuDTO getDailyMenu(Date date) {
		Menu menu = menuRepository.getDailyMenu(date);
		if(null == menu) {
			menu = new Menu();
			menu.setMenuDate(new Date(System.currentTimeMillis()));
			menu.setRecordTime(new Timestamp(System.currentTimeMillis()));
		}
		return MenuMapperImp.toDTO(menu);
	}


	
	
}
