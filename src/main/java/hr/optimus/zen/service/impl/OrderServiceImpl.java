package hr.optimus.zen.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.UserOrderRepositoryCustom;
import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.IngridientTypeWithCaloriesDTO;
import hr.optimus.zen.web.model.dto.MenuIngridientsDTO;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
	private final MenuRepository menuRepository;
	private final UserOrderRepositoryCustom userOrderRepository;
	private final UserRepositoryCustom userRepositroy;
	private final EmailService emailService;
	

	
	public UserOrder cancelOrder(UserBean user) {
		UserOrder userOrder = findUserOrder(user.getEmail());
		
		// refresh na browseru.. todo refactor
		if(userOrder.getId() == null) {
			return userOrder;
		}
		
		user.removeOrder(userOrder);
		userRepositroy.save(user);

		Menu dailyMenu = this.menuRepository.getDailyMenu(new Date(System.currentTimeMillis()));
		dailyMenu.setAvailable(
				Integer.valueOf(dailyMenu.getAvailable().intValue() + userOrder.getQuantity().intValue()));
		
		this.menuRepository.save(dailyMenu);
		
		emailService.sendCancleOrderEmail(user, userOrder);
		
		return userOrder;
	}


	public UserOrder orderMenu(UserOrder order, UserBean user) {
		
		UserOrder userOrder = null;
		Menu dailyMenu = getDailyMenu();
		if (dailyMenu.isAvailable(order.getQuantity())) {
			userOrder = new UserOrder(order.getMessage(), 
										order.getQuantity(), 
										order.getQuality(), 
										order.getAddress(),
										order.getCellular(), 
										dailyMenu, 
										user, 
										new Timestamp(System.currentTimeMillis()),
										Date.valueOf(LocalDate.now()));
			if (!user.getOrders().contains(userOrder)) {
				dailyMenu.setAvailable(
						Integer.valueOf(dailyMenu.getAvailable().intValue() - order.getQuantity().intValue()));
				this.menuRepository.save(dailyMenu);
				user.addOrder(userOrder);
				this.userRepositroy.save(user);
				
				emailService.sendOrderConfirmationEmail(userOrder);
			}
		} else {
			userOrder = new UserOrder();
			userOrder.setQuantity(new Integer(-1));
			userOrder.setMessage("Nemamo dovoljno raspolo�ivih obroka, probajte smanjiti koli?inu");
		}
		return userOrder;
	}


	public Menu getDailyMenu() {
		Menu m = this.menuRepository.getDailyMenu(new Date(System.currentTimeMillis()));
		if (m == null) {
			m = new Menu();
			m.setMenuDescription("Nothing so far, please come back later!");
		}
		return m;
	}


	/**
	 * ovo bi trebalo refaktorirat
	 * vraca listu a svaki korisnik u danu moze imat tocno jednu narudđbu
	 */

	public UserOrder findUserOrder(String email) {
		return this.userOrderRepository.getDailyOrderIfAny(email).size()>0 ?
				this.userOrderRepository.getDailyOrderIfAny(email).get(0):
					new UserOrder();
	}

	public List<UserOrder> getDailyUserOrders(Date date) {
		return this.userOrderRepository.getDailyOrders(date);
	}

	/**
	 * calculates daily menu nutrition
	 */
	public MenuIngridientsDTO getMenuNutrition() {
		Menu m = getDailyMenu();
		
		if(null == m.getTotalAvailable()) {
			return new MenuIngridientsDTO();
		}
		
		MenuIngridientsDTO dto = new MenuIngridientsDTO(new BigDecimal(m.getTotalAvailable()));
		
		dto.setIngridients(m.getIngridients().stream().map(mapper->{
			IngridientTypeWithCalories calories = mapper.getCalories();
			IngridientTypeWithCaloriesDTO dtoIngridient = new IngridientTypeWithCaloriesDTO();
			dtoIngridient.setType(calories.getType());
			dtoIngridient.setTotalCarbon(mapper.getQuantity().intValue(), calories.getCarbon().intValue());
			dtoIngridient.setTotalFat(mapper.getQuantity().intValue(), calories.getFat().intValue());
			dtoIngridient.setTotalProtein(mapper.getQuantity().intValue(), calories.getProteins().intValue());
			dtoIngridient.setTotalCalories(mapper.getQuantity().intValue(), calories.getCalories().intValue());
			dtoIngridient.setAproximatelyGrams(mapper.getQuantity().divide(dto.getTotalMenuAvailable(), RoundingMode.HALF_UP));
			return dtoIngridient;
		}).collect(Collectors.toList()));
		
		double total = dto.getIngridients().stream().mapToDouble(mapper->mapper.getTotalcalories().doubleValue()).sum();
		dto.setCaloriesTotal(new BigDecimal(total));
		dto.setCarbonTotal(new BigDecimal(dto.getIngridients().stream().mapToDouble(mapper->mapper.getTotalCarbon().doubleValue()).sum()));
		dto.setFatTotal(new BigDecimal(dto.getIngridients().stream().mapToDouble(mapper->mapper.getTotalFat().doubleValue()).sum()));
		dto.setProteinTotal(new BigDecimal(dto.getIngridients().stream().mapToDouble(mapper->mapper.getTotalProtein().doubleValue()).sum()));
		dto.total();
		return dto;
	}
}
