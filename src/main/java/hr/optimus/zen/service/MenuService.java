package hr.optimus.zen.service;

import java.sql.Date;
import java.util.List;

import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;

public interface MenuService {
	
	public MenuDTO insertMenu(MenuDTO menu);
	public MenuDTO getDailyMenu(Date date);
	public Menu deleteMenu(Menu menu);
	public List<Menu> getMenus(Date from, Date unitl);
	public MenuCalculationDTO getDailyTotal(Date date) throws Exception;
	public List<MenuCalculationDTO> getTotalBetweenDates(Date dateFrom, Date dateUntil) throws Exception;

}
