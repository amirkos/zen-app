package hr.optimus.zen.service;

import hr.optimus.zen.web.model.UserBean;

public interface UserService {
	
	UserBean findByEmail(String email);
	void deleteByEmail(String email);
	UserBean save(UserBean user);
	
	

}
