package hr.optimus.zen.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.MenuIngridientsDTO;

@Service
public interface OrderService {

	UserOrder orderMenu(UserOrder paramUserOrder, UserBean paramUserBean);

	UserOrder cancelOrder(UserBean paramUserBean);

	Menu getDailyMenu();

	MenuIngridientsDTO getMenuNutrition();

	UserOrder findUserOrder(String paramString);

	List<UserOrder> getDailyUserOrders(Date paramDate);
}
