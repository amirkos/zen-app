package hr.optimus.zen.service;

import hr.optimus.zen.mail.Mail;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;

public interface EmailService {
	
	void sendOrderConfirmationEmail(UserOrder paramUserOrder);

	void sendCancleOrderEmail(UserBean paramUserBean, UserOrder paramUserOrder);
	
	void sendRegistrationConfirmationEmail(UserBean bean);
	
	void sendMenuEmail(UserBean user, Menu menu);
	
	String TestsendSimpleMessage(Mail mail, String template);
	
	void sendErrorMail(String to,String subject, String text);
}
