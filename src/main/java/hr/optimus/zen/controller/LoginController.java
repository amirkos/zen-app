package hr.optimus.zen.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.config.Providers;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.security.service.AuthenticationFacadeService;
import hr.optimus.zen.security.social.providers.BaseProvider;
import hr.optimus.zen.security.social.providers.FacebookProvider;
import hr.optimus.zen.security.social.providers.GoogleProvider;
import hr.optimus.zen.security.social.providers.LinkedInProvider;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.web.model.LogInBean;
import hr.optimus.zen.web.model.UserBean;
import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class LoginController {
	
	private final FacebookProvider facebookProvider;
	private final GoogleProvider googleProvider;
	private final LinkedInProvider linkedInProvider;
	private final UserService userService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final BaseProvider baseProvider;
	private final EmailService emailService;
	
	private final AuthenticationFacadeService service;
	

	@RequestMapping(value = "/facebook", method = RequestMethod.GET)
	public String loginToFacebook(Model model, HttpServletResponse response) {
		
		return facebookProvider.getFacebookUserData(model, new UserBean(), response);
	}

	@RequestMapping(value = "/google", method = RequestMethod.GET)
	public String loginToGoogle(Model model,HttpServletResponse response) {
		return googleProvider.getGoogleUserData(model, new UserBean(), response);
	}

	@RequestMapping(value = "/linkedin", method = RequestMethod.GET)
	public String helloFacebook(Model model,HttpServletResponse response) {
		return linkedInProvider.getLinkedInUserData(model, new UserBean(), response);
	}

	@RequestMapping(value = { "/", ""," "})
	public String login(Model mav, LogInBean bean, HttpServletRequest request, HttpServletResponse response) {
		String ret = ZenAppUrlConstants.INDEX;
		if(service.isAuthenticated(request)) {
			ret=service.isAdmin(request)?
					ZenAppUrlConstants.REDIRECT_ADMIN:
						ZenAppUrlConstants.REDIRECT_USER;
		}else{ 
			mav.addAttribute("logInBean", bean);
		}
		return ret;
	}

	@GetMapping("/registration")
	public String showRegistration(UserBean userBean, Model mav) {
		mav.addAttribute("userBean", userBean);
		return ZenAppUrlConstants.REGISTER;
	}
	
	@GetMapping("/registration/confirmation")
	public String showRegistration(@RequestParam String email, @RequestParam String token, HttpServletResponse response) {
		UserBean bean = userService.findByEmail(email);
		if(bean == null || !bCryptPasswordEncoder.matches( bean.getEmail()+bean.getProvider(),token)) {
			return ZenAppUrlConstants.REDIRECT_INDEX;
		}
		bean.setActive(true);
		userService.save(bean);
		baseProvider.autoLoginUser(bean, response);
		
		return ZenAppUrlConstants.REDIRECT_USER;
	}
	
	@PostMapping("/registration")
	public String registerUser(HttpServletResponse httpServletResponse, Model model, @Valid UserBean userBean,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			List<String> errors = new ArrayList<String>();
			bindingResult.getAllErrors().stream().forEach(object->errors.add(object.getDefaultMessage()));//
			model.addAttribute("errors",errors);
			return ZenAppUrlConstants.REGISTER;
		}
		userBean.setProvider(Providers.REGISTRATION.toString());
		userBean.setActive(false);
		// Save the details in DB
		if (StringUtils.isNotEmpty(userBean.getPassword())) {
			userBean.setPassword(bCryptPasswordEncoder.encode(userBean.getPassword()));
		}
		UserBean user = userService.findByEmail(userBean.getEmail());
		
		if(null!=user) {
			model.addAttribute("confirmation", "Korisnik već postoji!");
			return ZenAppUrlConstants.REGISTER;

		}
		
		userService.save(userBean);
	
		emailService.sendRegistrationConfirmationEmail(userBean);
		
		model.addAttribute("confirmation", "Provjerite "+ userBean.getEmail()+" sanducic, kako bi dovrsili postupak registracije!");

		return ZenAppUrlConstants.REGISTER;//REDIRECT_USER;
	}

	@RequestMapping(value="/login",
			method = RequestMethod.POST
			)
	
	public ModelAndView loginUser(@ModelAttribute LogInBean data, HttpServletResponse response) {
		
		ModelAndView mav =null;
		UserBean bean = userService.findByEmail(data.getEmail());
		
		if(bean == null || 
				!bCryptPasswordEncoder.matches(data.getPassword(), bean.getPassword()) ||
				bean.getActive() == null || 
				!bean.getActive()){
			mav = new ModelAndView(ZenAppUrlConstants.INDEX);
			mav.getModelMap().addAttribute("error", "korisnik ne postoji, nije aktivan, ili nisu dobri podaci za logiranje!");
		}else{
			baseProvider.autoLoginUser(bean, response);
			mav = bean.getProvider().equals(ZenAppUrlConstants.ADMIN)?
					new ModelAndView(ZenAppUrlConstants.REDIRECT_ADMIN):
						new ModelAndView(ZenAppUrlConstants.REDIRECT_USER);
			
			mav.getModelMap().addAttribute("loggedInUser", bean);
			
		}
		return mav;
	}
	
	
	/** If we can't find a user/email combination */
	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return ZenAppUrlConstants.INDEX;
	}
	
	
	
	

}
