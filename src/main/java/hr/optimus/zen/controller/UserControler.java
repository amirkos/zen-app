package hr.optimus.zen.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.security.service.AuthenticationFacadeService;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequiredArgsConstructor
@Slf4j
public class UserControler {
	
	
	private final OrderService orderService;
	private final UserService userService;
	private final AuthenticationFacadeService auth;

	

	@PostMapping({ "/user/data" })
	public ModelAndView updateUserData(@ModelAttribute UserBean user) {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.REDIRECT_USER);
		UserBean userToSave = this.userService.findByEmail(this.auth.getAuthenticationEmail());
		userToSave.setAdresses(user.getAdresses());
		userToSave.setUserCellular(user.getUserCellular());
		this.userService.save(userToSave);
		return mav;
	}

	@RequestMapping({ "/user" })
	public ModelAndView showDailyMenu( UserOrder order) {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.USER_INDEX);
		UserBean user = this.userService.findByEmail(this.auth.getAuthenticationEmail());
		mav.addObject("loggedInUser", user);
		Menu dailyMenu = this.orderService.getDailyMenu();
		mav.addObject("menu", dailyMenu);
		mav.addObject("nutrition", orderService.getMenuNutrition());
		mav.addObject("order", this.orderService.findUserOrder(user.getEmail()));

		return mav;
	}

	@PostMapping({ "/user/order" })
	public ModelAndView makeOrder(UserOrder order) throws Exception {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.USER_INDEX);
		UserBean user = this.userService.findByEmail(this.auth.getAuthenticationEmail());
		mav.addObject("loggedInUser", user);

		UserOrder ret = this.orderService.orderMenu(order, user);
		Menu menu = this.orderService.getDailyMenu();
		mav.addObject("menu", menu);
		mav.addObject("nutrition", orderService.getMenuNutrition());
		mav.addObject("order", ret);

		return mav;
	}

	@PostMapping({"user/cancel" })
	public ModelAndView cancelDailyOrder() {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.USER_INDEX);
		UserBean user = this.userService.findByEmail(this.auth.getAuthenticationEmail());
		this.orderService.cancelOrder(user);
		mav.addObject("loggedInUser", user);
		mav.addObject("menu", this.orderService.getDailyMenu());
		mav.addObject("nutrition", orderService.getMenuNutrition());
		mav.addObject("order", new UserOrder());

		return mav;
	}
	@RequestMapping(value = "/unsubscirbe",
			method = RequestMethod.GET,
			produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public String unsubscribeUser(@RequestParam String email, @RequestParam String token, HttpServletRequest req) {
		String ret = null;
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		UserBean user = userService.findByEmail(email);
		if(null!=user && encoder.matches(user.getEmail()+user.getProvider(), token)) {
			userService.deleteByEmail(email);
			ret = "Uspjesno ste se odjavili s mailing liste!";
		}else {
			ret = "no soup 4 u, AND that's all we know :)";
			log.error("unSubscirbeUser-->ERROR:"+req.getLocalAddr() + " " + req.getQueryString());
		}
		return ret;
		
		
	}
	
	
}
