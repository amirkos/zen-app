package hr.optimus.zen.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.service.MenuService;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.OrderDTO;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Controller
@Getter
@Setter
@RequiredArgsConstructor
public class AdminController { 
	
	@Lazy
	private final MenuService menuService;
	private final OrderService orderService;
	private final IngridientTypeWithCaloriesRepository ingridients;
	
	
	@RequestMapping(value="/admin",  method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.ADMIN_INDEX);
		MenuDTO menu = menuService.getDailyMenu(new Date(System.currentTimeMillis()));
		mav.addObject("menu", menu);
		return mav;
		
		
	}
	@RequestMapping(value="/admin/menu", 
			method = RequestMethod.POST)
	public ModelAndView insertMenu(@ModelAttribute MenuDTO menu) {
		ModelAndView mav = new ModelAndView(ZenAppUrlConstants.REDIRECT_ADMIN);
		MenuDTO dto = menuService.insertMenu(menu);
		mav.addObject("menu", dto);  
		return mav;
	}
	
	@RequestMapping(value="/admin/menu/{menuId}", 
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getMenuById(@PathVariable String menuId) {

		/**
		 * todo get menu by id
		 */
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/admin/menu", 
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getMenuById() {

		return new ResponseEntity<Object>(menuService.getDailyMenu(new Date(System.currentTimeMillis())),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/admin/user/orders", 
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderDTO>> getDailyOrders() {
	        List<UserOrder> users = orderService.getDailyUserOrders(new Date(System.currentTimeMillis()));
	        
	        List<OrderDTO> user_orders = users.stream().map(m->{
	        	OrderDTO dto = new OrderDTO();
	        	dto.setEmail(m.getUser().getEmail());
	        	dto.setMessage(m.getMessage());
	        	dto.setQuantity(m.getQuantity());
	        	dto.setAdress(m.getAddress());
	        	dto.setCellular(m.getCellular());
	        	dto.setTime(new Date(m.getCreateDateTime().getTime()).toString());
	        	return dto;
	        }).collect(Collectors.toList());
	        
	        if (user_orders.isEmpty()) {
	            return new ResponseEntity<List<OrderDTO>>(HttpStatus.NO_CONTENT);
	            // You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<OrderDTO>>(user_orders, HttpStatus.OK);
	    }

	@RequestMapping(value="/admin/statistic", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuCalculationDTO> getDailyStatistic(@RequestParam("date") String date) throws Exception{
		MenuCalculationDTO dto = menuService.getDailyTotal(Date.valueOf(date));
		if (dto == null) {
	            return new ResponseEntity<MenuCalculationDTO>(HttpStatus.NO_CONTENT);
	    }
		
		return new ResponseEntity<MenuCalculationDTO>(dto, HttpStatus.OK);
		
		
	}
	
	@RequestMapping(value="/admin/ingridients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IngridientTypeWithCalories>> getAllingridients() throws Exception{
		
		 return 
				 new ResponseEntity<List<IngridientTypeWithCalories>>(ingridients.findAll()
						 			.stream()
						 			.sorted()
						 			.collect(Collectors.toList()), HttpStatus.OK);
		
		
	}
	
}
