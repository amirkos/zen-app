package hr.optimus.zen.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.context.annotation.Description;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.optimus.zen.config.ZenAppConfig;
import hr.optimus.zen.mail.Mail;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.service.impl.EmailServiceImpl;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import lombok.RequiredArgsConstructor;

@Description(value = "ovo je kontroler za testiranje mail templejta, ne sluzi za nista osim za to :)")
@Controller
@RequiredArgsConstructor
public class MailTesterController {
	
	private final ZenAppConfig config;
	private final UserService userService;
	private final EmailService service;
	
	
	@RequestMapping(value = "/test_confirm",
			method = RequestMethod.GET,
			produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String test() {
		UserBean user_1 = userService.findByEmail("amir.kos@gmail.com");
		
		UserOrder order = new UserOrder();
		order.setMessage("molim danas jedan bez ruzmarina");
		order.setAddress("mock adressa 1");//user_1.getAdresses().get(0)
		order.setCellular("mock cellular 2"); //user_1.getUserCellular().get(0)
		order.setQuantity(new Integer(2));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		
		Menu todayMenu = new Menu();
		todayMenu.setMenuDate(new Date(System.currentTimeMillis()));
		todayMenu.setAvailable(new Integer(20));
		todayMenu.setTotalAvailable(new Integer(20));
		todayMenu.setPrice(new Integer(40));
		
		todayMenu.setMenuDescription("Tikvice na naglo, povrtna riza, "
				+ "i gulas od king konga, maline na zarui i malo koromaca na posteljici od "
				+ "ruzmarina, molimo vas da narudbe predate do 22 sati");
		todayMenu.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setMenu(todayMenu);
		
		Mail mail = new Mail();
		mail.setFrom("a@k.com");
		mail.setTo(user_1.getEmail());
		mail.setSubject("Zahvaljujemo na naruđbi");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user_1.getFirstName());
		model.put("order", order);
		model.put("day", LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE, dd.MMMM.yyyy", new Locale("hr","HR"))));
		model.put("web", "www.mojslon.com");
		model.put("cellular", "mocking cellular");
		model.put("mail", "mocking_email");
		model.put("unsubscribe","mockin unsubscribe link");
		model.put("baseUrl",config.getWebAdres());
		model.put("logo", "logo");

		mail.setModel(model);
		
		return service.TestsendSimpleMessage(mail,EmailServiceImpl.OrderConfirmationEmailTemplate);
	}
	
	@RequestMapping(value = "/test_confirmation",
			method = RequestMethod.GET,
			produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String testConfirmationRegistration() {
		UserBean user_1 = userService.findByEmail("amir.kos@gmail.com");
		Mail mail = new Mail();
		mail.setFrom("a@k.com");
		mail.setTo(user_1.getEmail());
		mail.setSubject("Zen-k ~ Potvrda registracije");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user_1.getFirstName());
		model.put("baseUrl", config.getWebAdres());
		model.put("registration", "mocking usubscribe link");
		model.put("day", LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE, dd.MMMM.yyyy", new Locale("hr","HR"))));
		mail.setModel(model);
		
		return service.TestsendSimpleMessage(mail,EmailServiceImpl.registrationConfirmationEmailTemplate);
	}
	
	
	@RequestMapping(value = "/test_cancel",
			method = RequestMethod.GET,
			produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String testCancleOrder() {
		UserBean user_1 = userService.findByEmail("amir.kos@gmail.com");
		
		UserOrder order = new UserOrder();
		order.setMessage("molim danas jedan bez ruzmarina");
		order.setAddress("mock adressa 1");//user_1.getAdresses().get(0)
		order.setCellular("mock cellular 2"); //user_1.getUserCellular().get(0)
		order.setQuantity(new Integer(2));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		
		Menu todayMenu = new Menu();
		todayMenu.setMenuDate(new Date(System.currentTimeMillis()));
		todayMenu.setAvailable(new Integer(20));
		todayMenu.setTotalAvailable(new Integer(20));
		todayMenu.setPrice(new Integer(40));
		
		todayMenu.setMenuDescription("Tikvice na naglo, povrtna riza, "
				+ "i gulas od king konga, maline na zarui i malo koromaca na posteljici od "
				+ "ruzmarina, molimo vas da narudbe predate do 22 sati");
		todayMenu.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setMenu(todayMenu);
		
		Mail mail = new Mail();
		mail.setFrom("a@k.com");
		mail.setTo(user_1.getEmail());
		mail.setSubject("Zahvaljujemo na naruđbi");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user_1.getFirstName());
		model.put("order", order);
		model.put("day", LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE, dd.MMMM.yyyy", new Locale("hr","HR"))));
		model.put("web", "www.mojslon.com");
		model.put("cellular", "mocking cellular");
		model.put("mail", "mocking_email");
		model.put("unsubscribe","mockin unsubscribe link");
		model.put("baseUrl",config.getWebAdres());
		model.put("logo", "logo");

		mail.setModel(model);
		
		return service.TestsendSimpleMessage(mail,EmailServiceImpl.OrderCancleationEmailTemplate);
	}
	
	@RequestMapping(value = "/test_menu",
			method = RequestMethod.GET,
			produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String testMenu() {
		UserBean user_1 = userService.findByEmail("amir.kos@gmail.com");
		
		UserOrder order = new UserOrder();
		order.setMessage("molim danas jedan bez ruzmarina");
		order.setAddress("mock adressa 1");//user_1.getAdresses().get(0)
		order.setCellular("mock cellular 2"); //user_1.getUserCellular().get(0)
		order.setQuantity(new Integer(2));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		
		Menu todayMenu = new Menu();
		todayMenu.setMenuDate(new Date(System.currentTimeMillis()));
		todayMenu.setAvailable(new Integer(20));
		todayMenu.setTotalAvailable(new Integer(20));
		todayMenu.setPrice(new Integer(40));
		
		todayMenu.setMenuDescription("Tikvice na naglo, povrtna riza, "
				+ "i gulas od king konga, maline na zarui i malo koromaca na posteljici od "
				+ "ruzmarina, molimo vas da narudbe predate do 22 sati");
		todayMenu.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setMenu(todayMenu);
		
		Mail mail = new Mail();
		mail.setFrom("a@k.com");
		mail.setTo(user_1.getEmail());
		mail.setSubject("Zahvaljujemo na naruđbi");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", user_1.getFirstName());
		model.put("day", LocalDate.now()
				.format(DateTimeFormatter.ofPattern("EEEE, dd.MMMM.yyyy", new Locale("hr","HR"))));
		model.put("menu", todayMenu);
		model.put("web", "www.mojslon.com");
		model.put("cellular", "mocking cellular");
		model.put("mail", "mocking_email");
		model.put("unsubscribe",config.getWebAdres());
		model.put("baseUrl","http://localhost:8080");
		model.put("logo", "logo");

		mail.setModel(model);
		//service.sendMenuEmail(user_1, todayMenu);
		return service.TestsendSimpleMessage(mail,EmailServiceImpl.DailyMenuEmailTemplate);
	}

}
