package hr.optimus.zen.web.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.dto.IngridientDTO;


@Mapper(componentModel = "spring")
public interface IngridientMapper {
	
	IngridientMapper INSTANCE = Mappers.getMapper(IngridientMapper.class);
	
	
	Ingridient toEntity(IngridientDTO dto);
	
	IngridientDTO toDto(Ingridient entity);
	
			
			

}
