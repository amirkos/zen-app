package hr.optimus.zen.web.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LogInBean {
	
	private String email;
	private String password;

	
}
