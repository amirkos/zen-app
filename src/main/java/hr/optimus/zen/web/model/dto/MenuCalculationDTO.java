package hr.optimus.zen.web.model.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import groovy.transform.ToString;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ToString
public class MenuCalculationDTO {
	
	private String menuDescription;
	
	List<ConsumersDTO> consumers;
	
	List<IngridientDTO> ingridients;
	
	private BigDecimal totalSpent;
	
	private BigDecimal totalEarned;
	
	private Date menuDate;
	
	private String wholeDate;
	
	public void setLocaleDate(Date date) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddd", java.util.Locale.ENGLISH);
		Date myDate = sdf.parse(date.toString());
		sdf.applyPattern("EEE, d MMM yyyy");
		wholeDate = sdf.format(myDate);
	}
	

}
