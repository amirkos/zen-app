package hr.optimus.zen.web.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Email;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "user")
@Table(name = "registerd_user")
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "email")
public class UserBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull(message = "{email.not.empty}")
	@Email(message = "{email.format}")
	@Size(min = 3, max = 30, message = "{email.not.empty}")
	@Id
	private String email;
	@NotNull(message = "{first.name}")
	@Size(min = 3, max = 30, message = "{first.name}")
	private String firstName;
	@NotNull(message = "{last.name}")
	@Size(min = 3, max = 30, message = "{last.name}")
	private String lastName;
	private String title;
	private String country;
	private String password;
	@Transient
	private String passwordConfirm;
	private String provider;
	private String image;
	
	private Boolean active;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "user_adress", joinColumns = { @javax.persistence.JoinColumn(name = "email")  })
	@Column(name = "adress")
	@Fetch(FetchMode.SUBSELECT)
	private List<String> adresses = new ArrayList<String>();

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "user_cellular", joinColumns = { @javax.persistence.JoinColumn(name = "email") })
	@Column(name = "cellular")
	@Fetch(FetchMode.SUBSELECT)
	private List<String> userCellular = new ArrayList<String>();

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = {
			CascadeType.ALL }, orphanRemoval = true)
	private List<UserOrder> orders = new ArrayList<UserOrder>();

	public void addOrder(UserOrder order) {
		this.orders.add(order);
		order.setUser(this);
	}

	public void removeOrder(UserOrder order) {
		this.orders.remove(order);
		order.setUser(null);
	}
	
	

	
}
