package hr.optimus.zen.web.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Table(name = "ingridient")
@Entity
public  class Ingridient implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ingridient_id")
	private Long Id;
	private BigDecimal quantity;
	private BigDecimal price;
	
	@OneToOne
	@JoinColumn(name = "ingridient_type_id")
	private IngridientTypeWithCalories calories = new IngridientTypeWithCalories();
	
	public Ingridient(BigDecimal quantity, BigDecimal price) {
		super();
		this.quantity = quantity;
		this.price = price;
	}
	
	public Ingridient() {}
	
	
	
	
	
	

}
