package hr.optimus.zen.web.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "registerd_user_order")
@Getter
@Setter
@ToString(exclude = {"user"})
@NoArgsConstructor
@EqualsAndHashCode( of = {"createDate", "user"})
public class UserOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private String message;
	private Integer quantity;
	private Integer quality;
	private Timestamp createDateTime;
	private Date createDate;
	private String address;
	private String cellular;

	@OneToOne
	private Menu menu;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "email")
	private UserBean user;


	


	public UserOrder(String message, Integer quantity, Integer quality, String address, String cellular, Menu menu,
			UserBean user, Timestamp createDateTime, Date createDate) {
		this.message = message;
		this.quantity = quantity;
		this.quality = quality;
		this.createDateTime = createDateTime;
		this.createDate = createDate;
		this.address = address;
		this.cellular = cellular;
		this.menu = menu;
		this.user = user;
	}



}
