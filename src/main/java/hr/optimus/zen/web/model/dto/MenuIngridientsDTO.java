package hr.optimus.zen.web.model.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MenuIngridientsDTO {

	private List <IngridientTypeWithCaloriesDTO> ingridients;
	
	private BigDecimal fatTotal;
	private BigDecimal caloriesTotal;
	private BigDecimal carbonTotal;
	private BigDecimal proteinTotal;
	
	private BigDecimal totalMenuAvailable;
	
	
	public MenuIngridientsDTO(BigDecimal numberOfMenus) {
		fatTotal = BigDecimal.ZERO;
		caloriesTotal = BigDecimal.ZERO;
		carbonTotal = BigDecimal.ZERO;
		proteinTotal = BigDecimal.ZERO;
		totalMenuAvailable = numberOfMenus;
	}
	
	public void total() {
		fatTotal = fatTotal.divide(totalMenuAvailable, RoundingMode.HALF_UP);
		caloriesTotal = caloriesTotal.divide(totalMenuAvailable, RoundingMode.HALF_UP);
		carbonTotal = carbonTotal.divide(totalMenuAvailable, RoundingMode.HALF_UP);
		proteinTotal = proteinTotal.divide(totalMenuAvailable, RoundingMode.HALF_UP);
	}
	
	
	
	
}
