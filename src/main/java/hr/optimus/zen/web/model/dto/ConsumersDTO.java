package hr.optimus.zen.web.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumersDTO {
	
	private String quantity;
	private String email;
	private String firstName;
	private String surName;
	private String message;

}
