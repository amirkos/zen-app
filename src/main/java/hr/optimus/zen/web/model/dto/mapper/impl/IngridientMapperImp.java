package hr.optimus.zen.web.model.dto.mapper.impl;

import org.springframework.stereotype.Service;

import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.dto.IngridientDTO;
import hr.optimus.zen.web.model.dto.mapper.IngridientMapper;
import lombok.RequiredArgsConstructor;

//@Primary
@Service("IngridientMapperImp")
@RequiredArgsConstructor
public class IngridientMapperImp implements IngridientMapper {

	
	private final IngridientMapper mapper;
	
	@Override
	public Ingridient toEntity(IngridientDTO dto) {
		Ingridient ingridient = mapper.toEntity(dto);
		ingridient.getCalories().setId(dto.getIngridientType());
		return ingridient;
	}

	@Override
	public IngridientDTO toDto(Ingridient entity) {
		IngridientDTO dto = mapper.toDto(entity);
		dto.setIngridientTypeValue(entity.getCalories().getType());
		dto.setIngridientType(entity.getCalories().getId());
//		dto.setType("mali slon");
		return dto;
	}

}
