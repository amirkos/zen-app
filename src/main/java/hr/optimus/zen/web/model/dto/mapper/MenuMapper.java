package hr.optimus.zen.web.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.mapper.impl.IngridientMapperImp;

@Mapper(componentModel = "spring",uses = IngridientMapperImp.class)
public interface MenuMapper {
	
	MenuMapper INSTANCE = Mappers.getMapper(MenuMapper.class);
	
	public Menu toEntity(MenuDTO dto);
	public MenuDTO toDTO(Menu menu);

}
