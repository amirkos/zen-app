package hr.optimus.zen.web.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDTO {
	
	private String message;
	private Integer quantity;
	private String email;
	private String time;
	private String adress;
	private String cellular;
	

}
