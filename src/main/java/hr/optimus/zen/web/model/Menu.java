package hr.optimus.zen.web.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "menu")
@Getter
@Setter
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menu_id")
	private Long Id;
	private Date menuDate;
	private String menuDescription;
	private Integer available;
	private Integer totalAvailable;
	private Integer price;
	private Timestamp recordTime;
	@Column(length = 1024, name = "menu_facts")
	private String menuFacts;

	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "menu_id")
	private List<Ingridient> ingridients = new ArrayList<Ingridient>();

	public List<Ingridient> getIngridients() {
		return this.ingridients;
	}

	public boolean isAvailable(Integer orderNum) {
		return (this.available != null) && (this.available.intValue() > 0)
				&& (this.available.intValue() >= orderNum.intValue());
	}
}
