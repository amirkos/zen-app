package hr.optimus.zen.web.model.dto;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class MenuDTO {
	
	private Long Id;
	private Date menuDate;
	private String menuDescription;
	private Integer available;
	private Integer totalAvailable;
	private Integer price;
	private Timestamp recordTime;
	
	private String menuFacts;
	
	private Boolean sendEmail;

	private List<IngridientDTO> ingridients = new ArrayList<IngridientDTO>();
	

}
