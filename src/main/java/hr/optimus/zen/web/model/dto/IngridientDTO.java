package hr.optimus.zen.web.model.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngridientDTO {
	
	private BigDecimal quantity;
	private BigDecimal price;
	private Long ingridientType;
	private String ingridientTypeValue;
	
	

}
