package hr.optimus.zen.web.model.dto;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import lombok.Data;

@Data
public class IngridientTypeWithCaloriesDTO {
	
	private BigDecimal totalFat;
	private BigDecimal totalCarbon;
	private BigDecimal totalProtein;
	private BigDecimal totalcalories;
	
	private BigDecimal aproximatelyGrams;
	
	private String type;
	private BigDecimal hundred = new BigDecimal(100);
	
	/**
	 * gets calories per 1 gram of food
	 * @param num
	 * @return
	 */
	private BigDecimal getValuePerGram(int num) {
		BigDecimal ret = new BigDecimal(num);
		return ret.divide(hundred, new MathContext(6, RoundingMode.HALF_UP));
	}
	
	public void setTotalFat(int quantity, int fat) {
		totalFat = new BigDecimal(quantity).multiply(getValuePerGram(fat));
	}
	
	public void setTotalCarbon(int quantity, int fat) {
		totalCarbon = new BigDecimal(quantity).multiply(getValuePerGram(fat));
	}
	
	public void setTotalProtein(int quantity, int fat) {
		totalProtein = new BigDecimal(quantity).multiply(getValuePerGram(fat));
	}
	
	public void setTotalCalories(int quantity, int fat) {
		totalcalories = new BigDecimal(quantity).multiply(getValuePerGram(fat));
	}
	

	
	
	
	
	

}
