package hr.optimus.zen.web.model.dto.mapper.impl;

import org.springframework.stereotype.Service;

import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.mapper.MenuMapper;
import lombok.RequiredArgsConstructor;

@Service("MenuMapperImp")
@RequiredArgsConstructor
public class MenuMapperImp implements MenuMapper {

	
	private final MenuMapper mapper;
	
	@Override
	public Menu toEntity(MenuDTO dto) {
		Menu m = mapper.toEntity(dto);
		return m;
	}

	@Override
	public MenuDTO toDTO(Menu menu) {
		MenuDTO dto = mapper.toDTO(menu);
		return dto;
	}

	

}
