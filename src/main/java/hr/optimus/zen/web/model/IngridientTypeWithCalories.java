package hr.optimus.zen.web.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="ingridient_type_calories")
public class IngridientTypeWithCalories implements Comparable<IngridientTypeWithCalories> {

	@Id
	private Long Id;
	
	private String type;
	private Integer calories;
	private Integer proteins;
	private Integer carbon;
	private Integer fat;
	// private static int HUNDRED_GRAMS = 100;
	
	@Override
	public int compareTo(IngridientTypeWithCalories o) {
		return type.compareTo(o.getType());
	}
	
	
}
