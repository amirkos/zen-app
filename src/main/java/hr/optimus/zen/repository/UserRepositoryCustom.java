package hr.optimus.zen.repository;

import java.util.List;

import hr.optimus.zen.web.model.UserBean;

public interface UserRepositoryCustom {
	
	  UserBean save(UserBean paramUserBean);

	  UserBean findByEmail(String paramString);
	  
	  List<UserBean> findAllUsers();
	  
	  void deleteUserByEmail(String email);
}
