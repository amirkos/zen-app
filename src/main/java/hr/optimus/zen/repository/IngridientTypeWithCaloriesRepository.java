package hr.optimus.zen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.optimus.zen.web.model.IngridientTypeWithCalories;

@Repository
public interface IngridientTypeWithCaloriesRepository extends JpaRepository<IngridientTypeWithCalories, Long> {

}
