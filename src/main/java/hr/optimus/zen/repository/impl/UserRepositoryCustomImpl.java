package hr.optimus.zen.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import hr.optimus.zen.repository.UserRepository;
import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Repository
@Transactional
@RequiredArgsConstructor
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
	
	@PersistenceContext
	private final EntityManager entityManager;
	private final UserRepository userRepository;

	public UserBean save(UserBean user) {
		return (UserBean) this.userRepository.saveAndFlush(user);
	}

	public UserBean findByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	
	public List<UserBean> findAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public void deleteUserByEmail(String email) {
		userRepository.delete(email);
		
	}
}
