package hr.optimus.zen.repository.impl;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import hr.optimus.zen.repository.UserOrderRepository;
import hr.optimus.zen.repository.UserOrderRepositoryCustom;
import hr.optimus.zen.web.model.UserOrder;
import lombok.RequiredArgsConstructor;


@Repository
@Transactional
@RequiredArgsConstructor
public class UserOrderRepositoryCustomImpl implements UserOrderRepositoryCustom {
	 
	 @PersistenceContext
	 private final EntityManager entityManager;
	
	 private final UserOrderRepository userOrderRepository; 

	@Override
	public List<UserOrder> getDailyOrderIfAny(String email) {
		List<UserOrder> orders = userOrderRepository.getDailyOrders(new Date(System.currentTimeMillis()));
		return orders.stream().filter(filter -> {
			return filter.getUser().getEmail().equals(email);
		}).collect(Collectors.toList());
	}

	@Override
	public List<UserOrder> getDailyOrders(Date date) {
		return userOrderRepository.getDailyOrders(date);
	}
	
	public UserOrder save(UserOrder tosave) {
		return userOrderRepository.save(tosave);
	}

	@Override
	public UserOrder findOne(Long Id) {
		return userOrderRepository.findOne(Id);
	}

	@Override
	public void delete(Long Id) {
		userOrderRepository.delete(Id);
		
	}

	

}
