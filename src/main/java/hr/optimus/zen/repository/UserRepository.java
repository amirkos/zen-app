package hr.optimus.zen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.optimus.zen.web.model.UserBean;

@Repository
public interface UserRepository extends JpaRepository<UserBean, String> {

    UserBean findByEmail(String email);
}

