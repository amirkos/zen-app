package hr.optimus.zen.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import hr.optimus.zen.web.model.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {
	
	@Query("select d from Menu d where d.menuDate= :date")
	Menu getDailyMenu(@Param("date") Date date);
	
	

}
