package hr.optimus.zen.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import hr.optimus.zen.web.model.UserOrder;


public interface UserOrderRepositoryCustom {
	
	List<UserOrder> getDailyOrderIfAny( @Param ("email") String email); 
	
	List<UserOrder> getDailyOrders(  @Param ("date") Date date);
	
	UserOrder save(UserOrder tosave);
	
	UserOrder findOne(Long Id);
	
	void delete(Long Id);

}
