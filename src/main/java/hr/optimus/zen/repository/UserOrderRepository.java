package hr.optimus.zen.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import hr.optimus.zen.web.model.UserOrder;

@Repository
public interface UserOrderRepository extends JpaRepository<UserOrder, Long> {
	
	@Query("select d from UserOrder d inner join d.user ub where ub.email=:email" )
	List<UserOrder> getAllUserOrders( @Param ("email") String email);
	
	
	@Query("select d from UserOrder d where createDate=:date")
	List<UserOrder> getDailyOrders(  @Param ("date") Date date);
	
	

}
