package hr.optimus.zen.security;

import java.time.LocalDateTime;
import java.util.Collection;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class MyAccesDecisionManager implements AccessDecisionVoter<Object> {

	
	

	@Override
	public boolean supports(ConfigAttribute attribute) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return true;
	}




	@Override
	public int vote(Authentication authentication, Object object, Collection<ConfigAttribute> attributes) {
		return authentication.getAuthorities().stream()  
				.map(GrantedAuthority::getAuthority)
			      .filter(r -> "ROLE_USER".equals(r)  && LocalDateTime.now().getMinute() % 2 != 0)
			       
			      .findAny()
			      .map(s -> ACCESS_DENIED)
			      .orElseGet(() -> ACCESS_ABSTAIN);
	}

	

}
