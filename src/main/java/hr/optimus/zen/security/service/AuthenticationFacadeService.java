package hr.optimus.zen.security.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

public interface AuthenticationFacadeService {
	
	Authentication getAuthentication();
	String getAuthenticationEmail();
	String getAuthenticationPwd();
	boolean isAuthenticated(HttpServletRequest request);
	boolean isAdmin(HttpServletRequest request);
	 
}
