package hr.optimus.zen.security.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import hr.optimus.zen.config.Roles;
import hr.optimus.zen.security.service.AuthenticationFacadeService;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AuthenticationFacadeServiceImpl implements AuthenticationFacadeService {

	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	
	/**
	 * ovo treba popraviti testovi s @mock user nisu prolazili jel 
	 * taj ne popuni details na authentication objektu..
	 * 
	 * https://eliux.github.io/java/spring/testing/how-to-mock-authentication-in-spring/
	 */
	public String getAuthenticationEmail() {
		return SecurityContextHolder.getContext().getAuthentication().getDetails()==null?
				((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername():
					((UserBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getEmail();
	}
	
	public String getAuthenticationPwd() {
		return SecurityContextHolder.getContext().getAuthentication().getDetails()==null?
				((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPassword():
					((UserBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getPassword();
	}


	public boolean isAuthenticated(HttpServletRequest request) {
		Authentication user = SecurityContextHolder.getContext().getAuthentication();
		if (user instanceof UsernamePasswordAuthenticationToken) {
			return true;
		}

		return false;
	}

	@Override
	public boolean isAdmin(HttpServletRequest request) {

		Authentication user = SecurityContextHolder.getContext().getAuthentication();
		if (user instanceof UsernamePasswordAuthenticationToken) {
			if (user.getAuthorities().contains(new SimpleGrantedAuthority(Roles.ROLE_ADMIN.toString()))) {
				return true;
			}

		}
		return false;
	}

	

}
