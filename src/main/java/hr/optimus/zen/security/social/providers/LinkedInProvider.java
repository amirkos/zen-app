package hr.optimus.zen.security.social.providers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.LinkedInProfileFull;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import hr.optimus.zen.config.Providers;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class LinkedInProvider  {
	

    private final BaseProvider baseProvider ;

	public String getLinkedInUserData(Model model, UserBean userForm, HttpServletResponse response) {

		ConnectionRepository connectionRepository = baseProvider.getConnectionRepository();
		if (connectionRepository.findPrimaryConnection(LinkedIn.class) == null) {
			return ZenAppUrlConstants.REDIRECT_LOGIN;
		}
		populateUserDetailsFromLinkedIn(userForm);
		//Save the details in DB
		baseProvider.saveUserDetails(userForm);
		
		//Login the User
		baseProvider.autoLoginUser(userForm,response);
			
		model.addAttribute("loggedInUser",userForm);
		return ZenAppUrlConstants.REDIRECT_USER;
	}
	
	private void populateUserDetailsFromLinkedIn(UserBean userForm) {
		LinkedIn linkedIn = baseProvider.getLinkedIn();
		LinkedInProfileFull linkedInUser = linkedIn.profileOperations().getUserProfileFull();
		userForm.setEmail(linkedInUser.getEmailAddress());
		userForm.setFirstName(linkedInUser.getFirstName());
		userForm.setLastName(linkedInUser.getLastName());
		userForm.setImage(linkedInUser.getProfilePictureUrl());
		userForm.setProvider(Providers.LINKEDIN.toString());
	}

}
