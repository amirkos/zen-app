package hr.optimus.zen.security.social.providers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.google.api.Google;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import hr.optimus.zen.config.Providers;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GoogleProvider {
	private final BaseProvider baseProvider;
	private final UserRepositoryCustom userRepository;
	private final GoogleService service;

	public String getGoogleUserData(Model model, UserBean userForm,HttpServletResponse response) {
		ConnectionRepository connectionRepository = this.baseProvider.getConnectionRepository();
		if (connectionRepository.findPrimaryConnection(Google.class) == null) {
			return ZenAppUrlConstants.REDIRECT_LOGIN;
		}
		Connection<Google> conn = connectionRepository.findConnections(Google.class).get(0);
		//clear zato sto cuva konekcije
		connectionRepository.findAllConnections().clear();
		ConnectionData fbdata = conn.createData();
		String token = fbdata.getAccessToken();
		String id = fbdata.getProviderUserId();
		populateUserDetailsFromGoogle(userForm,id, token);
		if (null == this.userRepository.findByEmail(userForm.getEmail())) {
			this.baseProvider.saveUserDetails(userForm);
		}
		this.baseProvider.autoLoginUser(userForm,response);
	
		return ZenAppUrlConstants.REDIRECT_USER;
	}

	protected void populateUserDetailsFromGoogle(UserBean userForm, String id, String accesToken) {
		GoogleUserProfile googleUser = service.getProfile(id, accesToken);//google.plusOperations().getGoogleProfile();//
		userForm.setEmail(googleUser.getEmail());
		userForm.setFirstName(googleUser.getFirstName());
		userForm.setLastName(googleUser.getLastName());
		userForm.setImage(googleUser.getImageUrl());
		userForm.setActive(true);
		userForm.setProvider(Providers.GOOGLE.toString());
	}
}
