package hr.optimus.zen.security.social.providers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.google.api.Google;
import org.springframework.social.linkedin.api.LinkedIn;

import hr.optimus.zen.autologin.Autologin;
import hr.optimus.zen.repository.UserRepository;
import hr.optimus.zen.web.model.UserBean;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Configuration
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequiredArgsConstructor
@Data
public class BaseProvider {

	private final Facebook facebook;
	private final Google google;
	private final LinkedIn linkedIn;
	private final ConnectionRepository connectionRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final UserRepository userRepository;
	protected final Autologin autologin;

	protected void saveUserDetails(UserBean userBean) {

		userBean.setPassword(bCryptPasswordEncoder.encode(userBean.getEmail()));
		userBean.setActive(true);
		userRepository.save(userBean);
	}

	public void autoLoginUser(UserBean userBean, HttpServletResponse response) {
		autologin.setSecuritycontext(userBean);
		Cookie c = new Cookie("zen-app", bCryptPasswordEncoder.encode(userBean.getEmail() + userBean.getProvider()));
		c.setMaxAge(60*60);
		response.addCookie(c);
	}

	

}
