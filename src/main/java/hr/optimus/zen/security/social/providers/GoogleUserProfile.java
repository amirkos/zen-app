package hr.optimus.zen.security.social.providers;

import org.springframework.social.connect.UserProfile;

import lombok.Data;
@Data
public class GoogleUserProfile extends UserProfile {
	
	private String imageUrl;

	public GoogleUserProfile(String id,String name, String firstName, String lastName, String email, String username, String imgUrl) {
		super(id,name, firstName, lastName, email, username);
		this.imageUrl = imageUrl;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
