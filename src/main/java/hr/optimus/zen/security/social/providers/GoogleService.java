package hr.optimus.zen.security.social.providers;

import java.util.Arrays;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
public class GoogleService {
	
	private final RestTemplate restTemplate;

    public GoogleService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    //https://content-people.googleapis.com/v1/people/me?personFields=names%2Cphotos&key=AIzaSyAa8yy0GdcGPHdtD083HiGGx_S0vMPScDM
    public GoogleUserProfile getProfile(String id, String accessToken) {
    	try {
    		String params = "personFields=names,photos,emailAddresses";
    		
    		 //build url
            String url = "https://content-people.googleapis.com/v1/people/me?"+params;
            //create headers
            HttpHeaders headers = new HttpHeaders();
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            String bearerToken = "Bearer "+ accessToken;
            headers.put("Authorization", Arrays.asList(bearerToken));

            // create request
            HttpEntity request = new HttpEntity(headers);

            //use rest template
            ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.GET, request, String.class);

            //check for status code
            if (response.getStatusCode().is2xxSuccessful()) {
                JsonNode root =  new ObjectMapper().readTree(response.getBody());
                JsonNode names = root.get("names").get(0);
                JsonNode photos = root.get("photos").get(0);
                JsonNode email = root.get("emailAddresses").get(0);
                // return a user profile object
                return new GoogleUserProfile(
                		names.get("metadata").get("source").path("id").asText(), 
                		names.path("displayName").asText(),
                		names.path("givenName").asText(), 
                		names.path("familyName").asText(),
                		email.path("value").asText(),
                		email.path("value").asText(),
                		photos.get("url").asText()
                        );
            }
		} catch (Exception e) {
			 e.printStackTrace();
		}
    	
    	return null;
    }

}
