package hr.optimus.zen.security.social.providers;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import hr.optimus.zen.config.Providers;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.web.model.UserBean;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FacebookProvider {
	
	private final BaseProvider baseProvider;
	private final UserRepositoryCustom userRepository;
	// hack jel ne radi vise spring social
	private final FacebookService facebookService; 

	public String getFacebookUserData(Model model, UserBean userForm, HttpServletResponse response) {
		ConnectionRepository connectionRepository = this.baseProvider.getConnectionRepository();
		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
			return ZenAppUrlConstants.REDIRECT_LOGIN;
		}
		Connection<Facebook> conn = connectionRepository.findConnections(Facebook.class).get(0);
		connectionRepository.findAllConnections().clear();
		
		ConnectionData fbdata = conn.createData();
		String token = fbdata.getAccessToken();
		String id = fbdata.getProviderUserId();
		
		populateUserDetailsFromFacebook(userForm, id, token);

		this.baseProvider.autoLoginUser(userForm, response);
		if (null == this.userRepository.findByEmail(userForm.getEmail())) {
			this.baseProvider.saveUserDetails(userForm);
		}
		return ZenAppUrlConstants.REDIRECT_USER;
	}
	

	private void populateUserDetailsFromFacebook(UserBean userForm, String id,String accesToken) {
		UserProfile profile = facebookService.getProfile( id,accesToken);
		userForm.setEmail(profile.getEmail());
		userForm.setFirstName(profile.getFirstName());
		userForm.setLastName(profile.getLastName());
		userForm.setImage(facebookService.getProfilePicture(id));
		userForm.setActive(true);
		userForm.setProvider(Providers.FACEBOOK.toString());
	}
}
