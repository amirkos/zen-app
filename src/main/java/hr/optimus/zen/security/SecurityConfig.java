package hr.optimus.zen.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.expression.WebExpressionVoter;

import com.google.common.collect.Lists;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter { 

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub

		super.configure(auth);
	}

	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf()
				   .ignoringAntMatchers("/h2-console/**","/admin/**");
		
		http.authorizeRequests()
				 	.antMatchers("/css/**","/js/**","/img/**","/vendors/**", "/connect/**","/webjars/**","/registration/**").permitAll()
				 	.and()
				 	.authorizeRequests().antMatchers("/h2-console/**").permitAll()
				 	.and()
				 	.authorizeRequests().antMatchers("/error").authenticated()
				 	.and()
				 	.authorizeRequests().antMatchers("/user/**").hasAnyRole("FACEBOOK","GOOGLE","REGISTRATION")
				 	.and()
				 	.authorizeRequests().antMatchers("/admin/**").hasAnyRole("ADMIN")
				 	.and()
				.formLogin()
					.loginPage("/").permitAll()
					.defaultSuccessUrl("/").permitAll()
					.failureUrl("/login-error")
				.and()
		       
				.logout().logoutSuccessUrl("/").deleteCookies("JSESSIONID")
				.permitAll();
		
		http.headers().frameOptions().disable();
	}
	/**
	 * .and().rememberMe()
		            .alwaysRemember(true)
		            .tokenValiditySeconds(30*5)
		            .rememberMeCookieName("zen-app")
		            .key("somesecret")
		            .and()
	 * @return
	 */
	
	public AccessDecisionManager manager() {  
		List<AccessDecisionVoter<? extends Object>> decisionVoters = Lists.newArrayList(new RoleVoter(),new AuthenticatedVoter(), new WebExpressionVoter(), new MyAccesDecisionManager());
		UnanimousBased based = new UnanimousBased(decisionVoters);
		based.setAllowIfAllAbstainDecisions(true);
		return based;
	}
	
	public AccessDecisionManager manager1() {  
		List<AccessDecisionVoter<? extends Object>> decisionVoters = Lists.newArrayList( new MyAccesDecisionManager());
		UnanimousBased based = new UnanimousBased(decisionVoters);
//		based.setAllowIfAllAbstainDecisions(true);
		return based;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}

}

// @formatter:on