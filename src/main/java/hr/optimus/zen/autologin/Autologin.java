package hr.optimus.zen.autologin;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hr.optimus.zen.web.model.UserBean;

@Service
public class Autologin { 

	public void setSecuritycontext(UserBean userForm) {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+userForm.getProvider().toUpperCase()));
		UsernamePasswordAuthenticationToken token = new  UsernamePasswordAuthenticationToken(userForm.getEmail(),
				userForm.getPassword(), grantedAuthorities);
		token.setDetails(userForm);
		
		Authentication authentication = token;
		
		SecurityContextHolder.getContext().setAuthentication(authentication);   
	}
}
