package hr.optimus.zen.bootstrap;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import hr.optimus.zen.config.Providers;
import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.UserRepository;
import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;

@Component
@Profile("dev")
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {

	private UserRepository userRepository;
	private MenuRepository menuRepository;

	private IngridientTypeWithCaloriesRepository ingridientTypeRepository;

	private BCryptPasswordEncoder encoder;

	DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;

	@Autowired
	public DevBootStrap(UserRepository userRepository, MenuRepository menuRepository,
			 IngridientTypeWithCaloriesRepository ingridientTypeRepository,
			BCryptPasswordEncoder encoder) {
		super();
		this.userRepository = userRepository;
		this.menuRepository = menuRepository;
		this.ingridientTypeRepository = ingridientTypeRepository;
		this.encoder = encoder;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		initData();

	}
	
	private void createAdmin() {
		
		UserBean u1 = new UserBean();
		u1.setEmail("iskra.radinovic@gmail.com");
		u1.setFirstName("Iskra");
		u1.setLastName("Radinovic");
		u1.setPassword(encoder.encode("xxx"));
		u1.setProvider(Providers.ADMIN.toString());
		u1.setActive(true);
		u1.setAdresses(Lists.newArrayList("Adresa 11", "Adresa 22").stream().collect(Collectors.toList()));
		u1.setUserCellular(Lists.newArrayList("+385993176666").stream().collect(Collectors.toList()));

		userRepository.save(u1);

		
	}

	private void createUsers() {
		UserBean u = new UserBean();
		u.setEmail("amir.kos@webringapps.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword(encoder.encode("xxx"));
		u.setProvider(Providers.REGISTRATION.toString());
		u.setActive(true);
		u.setAdresses(Lists.newArrayList("Vojnoviceva 26").stream().collect(Collectors.toList()));
		u.setUserCellular(Lists.newArrayList("+385993173689").stream().collect(Collectors.toList()));

		userRepository.save(u);

		
		UserBean u2 = new UserBean();
		u2.setEmail("amir.kos@gmail.com");
		u2.setFirstName("Amir");
		u2.setLastName("Kos");
		u2.setPassword(encoder.encode("amir.kos@gmail.com"));
		u2.setProvider(Providers.GOOGLE.toString());
		u2.setActive(true);
		u2.setAdresses(
				Lists.newArrayList("Adresa 777").stream().collect(Collectors.toList()));
		u2.setUserCellular(Lists.newArrayList("+385993176666").stream().collect(Collectors.toList()));

		userRepository.save(u2);

		UserBean u3 = new UserBean();
		u3.setEmail("iskra@webringapps.com");
		u3.setFirstName("Iskra");
		u3.setLastName("Radinovic");
		u3.setPassword(encoder.encode("iskra@webringapps.com"));
		u3.setProvider(Providers.GOOGLE.toString());
		u3.setActive(true);
		u3.setAdresses(Lists.newArrayList("Adresa 11", "Adresa 22", "Adresa 22").stream().collect(Collectors.toList()));

		u3.setUserCellular(Lists.newArrayList("+385993176666").stream().collect(Collectors.toList()));
		
		userRepository.save(u3);
		
		 u3 = new UserBean();
		u3.setEmail("vojnoviceva26@gmail.com");
		u3.setFirstName("Adrijan");
		u3.setLastName("Kos");
		u3.setPassword(encoder.encode("vojnoviceva26@gmail.com"));
		u3.setProvider(Providers.GOOGLE.toString());
		u3.setActive(true);
		u3.setAdresses(Lists.newArrayList("Adresa 11", "Adresa 22", "Adresa 22").stream().collect(Collectors.toList()));
		u3.setUserCellular(Lists.newArrayList("+385993176888").stream().collect(Collectors.toList()));

		userRepository.save(u3);
	}
	
	private void createIngridientsTypeWithCalories() {
		
		for(int i = 1; i<=100; ++i) {
			IngridientTypeWithCalories cal = new IngridientTypeWithCalories();
			cal.setId(new Long(i));cal.setCalories(100+i); cal.setProteins(100+i); cal.setCarbon(100+i); cal.setFat(100+i); cal.setType("MESO" + i);
			ingridientTypeRepository.save(cal);
		}
		
	}
	
	private List<Ingridient> addAll(int num){
		List<Ingridient> ret = new ArrayList<Ingridient>();
		for(int i = 1; i<=num; ++i) {
			Ingridient ingridient = new Ingridient(new BigDecimal(i), new BigDecimal(i));
			ingridient.getCalories().setId(ingridientTypeRepository.findOne(new Long(i)).getId());
			ret.add(ingridient);
		}
		return ret;
		
	}

	private void createMenus() {
		
		Menu todayMenu = new Menu();
		todayMenu.setMenuDate(new Date(System.currentTimeMillis()));
		todayMenu.setAvailable(new Integer(20));
		todayMenu.setTotalAvailable(new Integer(20));
		todayMenu.setPrice(new Integer(40));
		
		todayMenu.setMenuDescription("Tikvice na naglo, povrtna riza, i gulas od king konga");
		todayMenu.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		todayMenu.setIngridients(addAll(10));// .stream().collect(Collectors.toSet()));

		menuRepository.save(todayMenu);
		
		Menu m = new Menu();
		m.setMenuDate(Date.valueOf("2017-01-02"));
		m.setAvailable(new Integer(20));
		m.setTotalAvailable(new Integer(20));
		m.setPrice(new Integer(40));
		m.setMenuDescription("Tikvice na naglo, povrtna riza, i gulas od king konga");
		m.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		m.setIngridients(addAll(5));// .stream().collect(Collectors.toSet()));

		menuRepository.save(m);

		Menu mm = new Menu();
		mm.setMenuDate(Date.valueOf("2011-01-01"));
		mm.setAvailable(new Integer(20));
		mm.setTotalAvailable(new Integer(20));
		mm.setPrice(new Integer(37));
		mm.setMenuDescription("Ragu od bivola na naglo, mungo grah riza, i KURAC");
		mm.setRecordTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		mm.setIngridients(addAll(3));// .stream().collect(Collectors.toSet()));

		menuRepository.save(mm);
	}
	
	private void deleteUserOrder() {
		UserBean user_1 = userRepository.findByEmail("amir.kos@gmail.com");
		
		System.out.println("amir.kos@gmail.com: broj narudzbi:"+user_1.getOrders().size());
		UserOrder uo = user_1.getOrders().get(0);
		user_1.removeOrder(uo);
		userRepository.saveAndFlush(user_1);
		
		user_1 = userRepository.findByEmail("amir.kos@gmail.com");
		
		System.out.println("amir.kos@gmail.com: broj narudzbi poslje brisanja:"+user_1.getOrders().size());
		user_1.getOrders().forEach(m->m.toString());

	}
	

	private void createUserOrders() {

		UserBean user_1 = userRepository.findByEmail("amir.kos@gmail.com");
		
		UserOrder order = new UserOrder();
		order.setMessage("molim danas jedan bez ruzmarina");
		order.setAddress(user_1.getAdresses().get(0));
		order.setCellular(user_1.getUserCellular().get(0));
		order.setQuantity(new Integer(2));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		
		Menu m = menuRepository.getDailyMenu(Date.valueOf("2011-01-01"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);

		user_1.addOrder(order);
		userRepository.save(user_1);
		

		menuRepository.save(m);

		
		UserBean user_2 = userRepository.findByEmail("amir.kos@webringapps.com");
		order = new UserOrder();
		
		order.setMessage("molim danas jedan bez ruzmarina 22");
		order.setAddress(user_2.getAdresses().get(0));
		order.setCellular(user_2.getUserCellular().get(0));
		order.setQuantity(new Integer(3));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		
		m = menuRepository.getDailyMenu(Date.valueOf("2011-01-01"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);
		
		user_2.addOrder(order);
		userRepository.save(user_2);
//		orderRepository.save(order);
		menuRepository.save(m);

		
		UserBean user_3 = userRepository.findByEmail("iskra.radinovic@gmail.com");
		order = new UserOrder();
		order.setMessage("molim danas jedan bez oraha");
		order.setAddress(user_3.getAdresses().get(0));
		order.setCellular(user_3.getUserCellular().get(0));
		order.setQuantity(new Integer(5));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		m = menuRepository.getDailyMenu(Date.valueOf("2011-01-01"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);
		
		user_3.addOrder(order);
		userRepository.save(user_3);
		menuRepository.save(m);

		
		UserBean user_4 = userRepository.findByEmail("iskra@webringapps.com");
		order = new UserOrder();
//		order.setUser(userRepository.findByEmail("iskra@webringapps.com"));
		order.setMessage("molim danas jedan bez lavandina");
		
		order.setAddress(user_4.getAdresses().get(0));
		order.setCellular(user_4.getUserCellular().get(0));

		
		order.setQuantity(new Integer(4));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2011-01-01").getTime()));
		order.setCreateDate(Date.valueOf("2011-01-01"));
		m = menuRepository.getDailyMenu(Date.valueOf("2011-01-01"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);
		
		user_4.addOrder(order);
		userRepository.save(user_4);
//		orderRepository.save(order);
		menuRepository.save(m);

		//////
		
		UserBean user_5 = userRepository.findByEmail("amir.kos@gmail.com");

		order = new UserOrder();
//		order.setUser(userRepository.findByEmail("amir.kos@gmail.com"));
		order.setMessage("molim danas jedan bez kadulje i tikvica");
		
		order.setAddress(user_5.getAdresses().get(0));
		order.setCellular(user_5.getUserCellular().get(0));
		order.setQuantity(new Integer(2));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setCreateDate(Date.valueOf("2017-01-02"));
//		u.getOrders().add(order);
		m = menuRepository.getDailyMenu(Date.valueOf("2017-01-02"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);
		
		user_5.addOrder(order);
		userRepository.save(user_5);
//		orderRepository.save(order);
		menuRepository.save(m);

		
		UserBean user_6 = userRepository.findByEmail("amir.kos@webringapps.com");
		order = new UserOrder();
		order.setUser(userRepository.findByEmail("amir.kos@webringapps.com"));
		order.setMessage("molim danas jedan bez kadulje i tikvica");
		
		order.setAddress(user_6.getAdresses().get(0));
		order.setCellular(user_6.getUserCellular().get(0));
		
		order.setQuantity(new Integer(4));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setCreateDate(Date.valueOf("2017-01-02"));
//		u.getOrders().add(order);
		m = menuRepository.getDailyMenu(Date.valueOf("2017-01-02"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);

		user_6.addOrder(order);
		userRepository.save(user_6);
		
//		orderRepository.save(order);
		menuRepository.save(m);
		
		UserBean user_7 = userRepository.findByEmail("iskra.radinovic@gmail.com");

		order = new UserOrder();
//		order.setUser(userRepository.findByEmail("iskra.radinovic@gmail.com"));
		order.setMessage("molim danas jedan bez kadulje i tikvica");
		order.setAddress(user_7.getAdresses().get(0));
		order.setCellular(user_7.getUserCellular().get(0));
		order.setQuantity(new Integer(3));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setCreateDate(Date.valueOf("2017-01-02"));
//		u.getOrders().add(order);
		m = menuRepository.getDailyMenu(Date.valueOf("2017-01-02"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());
		order.setMenu(m);
		
		user_7.addOrder(order);
		userRepository.save(user_7);

//		orderRepository.save(order);
		menuRepository.save(m);

		
		UserBean user_8 = userRepository.findByEmail("iskra@webringapps.com");
		order = new UserOrder();
//		order.setUser(userRepository.findByEmail("iskra@webringapps.com"));
		order.setMessage("molim danas jedan bez kadulje i sezama");
		
		order.setAddress(user_8.getAdresses().get(0));
		order.setCellular(user_8.getUserCellular().get(0));
		order.setQuantity(new Integer(6));
		order.setCreateDateTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		order.setCreateDate(Date.valueOf("2017-01-02"));
//		u.getOrders().add(order);
		m = menuRepository.getDailyMenu(Date.valueOf("2017-01-02"));
		m.setAvailable(m.getAvailable().intValue() - order.getQuantity().intValue());

		order.setMenu(m);
		user_8.addOrder(order);
		userRepository.save(user_8);
//		orderRepository.save(order);
		menuRepository.save(m);

	}
	
	



	private void initData() {
		createUsers();
		createIngridientsTypeWithCalories();
		createAdmin();
		createMenus();
		createUserOrders();
		deleteUserOrder();
		
//		loadUser();
//		deletOrder();

	}
	

}
