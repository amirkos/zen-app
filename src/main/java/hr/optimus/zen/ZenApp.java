package hr.optimus.zen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import hr.optimus.zen.config.HerokuIdler;

@SpringBootApplication
@EnableAsync
@EnableEncryptableProperties
@PropertySource("classpath:application.properties")
@EnableScheduling
public class ZenApp {
	
	static {
        System.setProperty("jasypt.encryptor.password", "supersecretz");
    }
	
	public static void main(String[] args) {
		SpringApplication.run(ZenApp.class, args);    
	}
	
	@Bean
	public HerokuIdler herokuNotIdle(){
	    return new HerokuIdler();
	}
}
      