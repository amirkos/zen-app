package hr.optimus.zen.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.service.MenuService;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.IngridientDTO;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.OrderDTO;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerTest {
	@Mock
	private IngridientTypeWithCaloriesRepository ingridients;
	@Mock
	private MenuService menuService;
	@Mock
	private OrderService orderService;
	@InjectMocks
	private AdminController adminController;

	@Before
	public void setUp() throws Exception {
	}
	
	private MenuDTO getMenuDTO() {
		MenuDTO menu = new MenuDTO();
		menu.setAvailable(new Integer(30));
		menu.setPrice(new Integer(50));
		menu.setMenuDate(Date.valueOf(LocalDate.now()));
		menu.setMenuDescription("Fino varivo s prokulicama");
		menu.setMenuFacts("Od prokulica imate bolju probavu");
		menu.setSendEmail(false);
		menu.setIngridients(createMenuIngridients());
		return menu;
	}

	private List<IngridientDTO> createMenuIngridients() {

		List<IngridientDTO> ret = new ArrayList<IngridientDTO>();
		for (int i = 1; i <= 100; ++i) {
			IngridientDTO dto = new IngridientDTO();
			dto.setPrice(new BigDecimal(i));
			dto.setQuantity(new BigDecimal(1000+i));
			dto.setIngridientType(new Long(i));
			dto.setIngridientTypeValue("POVRCE"+i);
			ret.add(dto);
		}
		return ret;
	}

	@Test
	public void testIndex() throws Exception {
		
		MenuDTO menu = getMenuDTO();
		when(menuService.getDailyMenu(any())).thenReturn(menu);
		
		ModelAndView mav = adminController.index();
		
		assertEquals(mav.getModel().containsKey("menu"), true);
		assertEquals(menu.getAvailable(), ((MenuDTO)mav.getModel().get("menu")).getAvailable());
		
		verify(menuService, times(1)).getDailyMenu(any());
		
	}

	@Test
	public void testInsertMenu() throws Exception {
		MenuDTO menu = getMenuDTO();
		when(menuService.insertMenu(menu)).thenReturn(menu);
		
		ModelAndView mav = adminController.insertMenu(menu);
		
		assertEquals(mav.getModel().containsKey("menu"), true);
		assertEquals(menu.getAvailable(), ((MenuDTO)mav.getModel().get("menu")).getAvailable());
		
		verify(menuService, times(1)).insertMenu(any());
		
	}
	
	private List<UserOrder> generateUserOrder(){
		Stream<UserOrder> lst = IntStream.range(0, 10).mapToObj(mapper ->{
			
			UserOrder order = new UserOrder();
			order.setMessage("message:"+ mapper);
			order.setQuantity(mapper);
			order.setAddress("adressa"+ mapper);
			order.setCellular("cellular"+ mapper);
			order.setCreateDateTime(new Timestamp(System.currentTimeMillis()));
			UserBean user = new UserBean();
			user.setEmail("mali_slon@gmail.com"+mapper);
			order.setUser(user);
			return order;
		});
		
		return lst.collect(Collectors.toList());
	}
	
	

	@Test
	public void testGetDailyOrders() throws Exception {
		List<UserOrder> lst = generateUserOrder();
		
		when(orderService.getDailyUserOrders(any())).thenReturn(lst);
		
		ResponseEntity<List<OrderDTO>> ret = adminController.getDailyOrders();
		verify(orderService, times(1)).getDailyUserOrders(any());
		assertEquals(ret.getStatusCode(), HttpStatus.OK);
		
	}

	@Test
	public void testgetDailyStatistic() throws Exception {
		MenuCalculationDTO dto = new MenuCalculationDTO();
		when(menuService.getDailyTotal(any())).thenReturn(dto);
		ResponseEntity<MenuCalculationDTO> ret = adminController.getDailyStatistic("2011-01-01");
		
		verify(menuService,times(1)).getDailyTotal(any());
		 assertEquals(ret.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void testgetAllingridients() throws Exception {
		List<IngridientTypeWithCalories> arr = new ArrayList<IngridientTypeWithCalories>();
		when(ingridients.findAll()).thenReturn(arr);
		ResponseEntity<List<IngridientTypeWithCalories>> ret = adminController.getAllingridients();
		verify(ingridients,times(1)).findAll();
		assertEquals(ret.getStatusCode(), HttpStatus.OK);
		 
	}



}
