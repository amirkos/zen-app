package hr.optimus.zen.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import hr.optimus.zen.autologin.Autologin;
import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.security.social.providers.BaseProvider;
import hr.optimus.zen.security.social.providers.FacebookProvider;
import hr.optimus.zen.security.social.providers.GoogleProvider;
import hr.optimus.zen.security.social.providers.LinkedInProvider;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.web.model.LogInBean;
import hr.optimus.zen.web.model.UserBean;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {
	
	@Mock
	private FacebookProvider facebookProvider;

	@Mock
	private GoogleProvider googleProvider;

	@Mock
	private LinkedInProvider linkedInProvider;

	@Mock
	private UserService userService;
	
	@Mock
	private MenuRepository menu;

	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Mock
	private Autologin autologin;
	
	@Mock
	private MockHttpServletResponse response;
	
	@Mock
	private  BaseProvider baseProvider;
	
	@Mock
	private BindingResult bindingResult;
	
	@Mock
	private EmailService emailService;
	
	
	private Model model;

	@InjectMocks
	private LoginController loginController;

	@Before
	public void setUp() throws Exception {
		model = new ExtendedModelMap();
	}

	@Test
	public void testLoginUser() throws Exception {
		
		UserBean bean = new UserBean();
		bean.setEmail("amir.kos@gmail.com");
		bean.setPassword(bCryptPasswordEncoder.encode("xxx"));
		bean.setProvider("USER");
		bean.setActive(true);
		
		LogInBean loginBean = new LogInBean();
		loginBean.setEmail("amir.kos@gmail.com");
		loginBean.setPassword("xxx");

		when(userService.findByEmail(anyString())).thenReturn(bean);
		when(bCryptPasswordEncoder.matches(loginBean.getPassword(), bean.getPassword())).thenReturn(true);
//		when(baseProvider.autoLoginUser(bean, response));
	    doNothing().when(baseProvider).autoLoginUser(bean, response);
	    doNothing().when(autologin).setSecuritycontext(bean);
	    
	    
		ModelAndView view = loginController.loginUser(loginBean,response);
		
		verify(userService, times(1)).findByEmail(anyString());
		verify(baseProvider, times(1)).autoLoginUser(anyObject(), anyObject());
//		verify(autologin, times(1)).setSecuritycontext(anyObject());
		
		assertEquals(view.getViewName(), "redirect:/user");
		assertEquals(view.getModel().containsKey("loggedInUser"), true);
		
		UserBean totest = (UserBean) view.getModel().get("loggedInUser");
		assertEquals(totest.getEmail(), bean.getEmail());
		assertEquals(totest.getPassword(), bean.getPassword());
		
	}
	
	@Test
	public void testRegistrationUserExist() {
		UserBean bean = new UserBean();
		bean.setEmail("amir.kos@gmail.com");
		bean.setFirstName("amir");
		bean.setLastName("kos");
		bean.setPassword("xxx");
		bean.setProvider("REGISTRATION");
		
		when(userService.findByEmail(anyString())).thenReturn(bean);
		
		String register = loginController.registerUser(response, model, bean, bindingResult);
		verify(userService, times(1)).findByEmail(bean.getEmail());
		
		assertEquals(register, ZenAppUrlConstants.REGISTER);
		assertEquals(model.containsAttribute("confirmation"), true);
		assertEquals(model.asMap().get("confirmation"), "Korisnik već postoji!");
		
	}
	
	@Test
	public void testRegistrationUserNotExists() {
		UserBean bean = new UserBean();
		bean.setEmail("amir.kos@gmail.com");
		bean.setFirstName("amir");
		bean.setLastName("kos");
		bean.setPassword("xxx");
		bean.setProvider("REGISTRATION");
		
		when(userService.findByEmail(anyString())).thenReturn(null);
		when(userService.save(bean)).thenReturn(bean);
		doNothing().when(emailService).sendRegistrationConfirmationEmail(bean);
		
		String register = loginController.registerUser(response, model, bean, bindingResult);

		verify(userService, times(1)).findByEmail(bean.getEmail());
		verify(emailService, times(1)).sendRegistrationConfirmationEmail(bean);
		
		assertEquals(register, ZenAppUrlConstants.REGISTER);
		assertEquals(bean.getActive(), false);
		assertEquals(model.containsAttribute("confirmation"), true);
		assertEquals(model.asMap().get("confirmation"), "Provjerite "+ bean.getEmail() +" sanducic, kako bi dovrsili postupak registracije!");
		
	}

}
