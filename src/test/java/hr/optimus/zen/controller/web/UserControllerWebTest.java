package hr.optimus.zen.controller.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.sql.Date;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.MenuIngridientsDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerWebTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	@MockBean
	OrderService orderService;
	@MockBean
	UserService userService;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	@Test
	public void testUserUrlWithNotLoggedUser() throws Exception {
		mvc.perform(get("/user").contentType(MediaType.TEXT_HTML)).andExpect(status().is3xxRedirection());
	}

	@WithMockUser(username  = "amir.kos@wba.com", password = "xxx", roles = "REGISTRATION")
	@Test
	public void testUpdateUserData() throws Exception {
		UserBean user = new UserBean();
		user.setAdresses(Arrays.asList("adress 1"));
		user.setUserCellular(Arrays.asList("cellular 1"));
		when(userService.save(user)).thenReturn(user);
		
		when(userService.findByEmail(any())).thenReturn(user);

		mvc.perform(post("/user/data").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andExpect(status().is3xxRedirection());
		
		verify(userService, times(1)).save(any());
	}

	@WithMockUser(value = "amir.kos@wba.com", password = "xxx", roles = "REGISTRATION")
	@WithUserDetails()
	@Test
	public void testShowDailyMenu() throws Exception {

		UserBean user = new UserBean();
		user.setEmail("amir.kos@wba.com");
		when(userService.findByEmail(user.getEmail())).thenReturn(user);
		UserOrder order = new UserOrder();
		when(orderService.findUserOrder(any())).thenReturn(order);
		Menu menu = new Menu();
		menu.setMenuDate(new Date(System.currentTimeMillis()));
		when(orderService.getDailyMenu()).thenReturn(menu);
		MenuIngridientsDTO dto = new MenuIngridientsDTO();
		when(orderService.getMenuNutrition()).thenReturn(dto);

		mvc.perform(get("/user").contentType(MediaType.TEXT_HTML)).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(model().attributeExists("menu"))
				.andExpect(model().attributeExists("nutrition")).andExpect(model().attributeExists("order"))
				.andExpect(view().name(ZenAppUrlConstants.USER_INDEX));
	}

	@WithMockUser(value = "amir.kos@wba.com", password = "xxx", roles = "REGISTRATION")
	@Test
	public void testMakeOrder() throws Exception {

		UserBean user = new UserBean();
		user.setEmail("amir.kos@wba.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		when(userService.findByEmail(user.getEmail())).thenReturn(user);
		UserOrder order = new UserOrder();
		when(orderService.orderMenu(any(), any())).thenReturn(order);
		Menu menu = new Menu();
		menu.setMenuDate(new Date(System.currentTimeMillis()));
		when(orderService.getDailyMenu()).thenReturn(menu);
		MenuIngridientsDTO dto = new MenuIngridientsDTO();
		when(orderService.getMenuNutrition()).thenReturn(dto);

		mvc.perform(post("/user/order").with(csrf()).contentType(MediaType.TEXT_HTML))

				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("loggedInUser"))
				.andExpect(model().attributeExists("menu"))
				.andExpect(model().attributeExists("nutrition"))
				.andExpect(model().attributeExists("order"))
		    	.andExpect(view().name(ZenAppUrlConstants.USER_INDEX))
		    	.andExpect(model().size(5));
		
		verify(orderService, times(1)).orderMenu(any(), any());
	}

	@WithMockUser(value = "amir.kos@wba.com", password = "xxx", roles = "REGISTRATION")
	@Test
	public void testCancelDailyOrder() throws Exception{
		
		UserBean user = new UserBean();
		user.setEmail("amir.kos@wba.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		when(userService.findByEmail(user.getEmail())).thenReturn(user);
		UserOrder order = new UserOrder();
		order.setCreateDate(Date.valueOf("2010-01-01"));
		when(orderService.cancelOrder(any())).thenReturn(order);
		MenuIngridientsDTO dto = new MenuIngridientsDTO();
		when(orderService.getMenuNutrition()).thenReturn(dto);
		
		Menu menu = new Menu();
		menu.setMenuDate(new Date(System.currentTimeMillis()));
		when(orderService.getDailyMenu()).thenReturn(menu);
		

		ResultActions result = mvc.perform(post("/user/cancel").with(csrf()).contentType(MediaType.TEXT_HTML))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(model().attributeExists("loggedInUser"))
				.andExpect(model().attributeExists("menu"))
				.andExpect(model().attributeExists("nutrition"))
				.andExpect(model().attributeExists("order"))
		    	.andExpect(view().name(ZenAppUrlConstants.USER_INDEX))
		    	.andExpect(model().size(4));
		
		verify(orderService, times(1)).cancelOrder(any());
	}

	@WithMockUser(value = "amir.kos@wba.com", password = "xxx", roles = "REGISTRATION")
	@Test
	public void testUnsubscribeUser() throws Exception {
		UserBean user = new UserBean();
		user.setEmail("amirkos@wba.com");
		user.setProvider("REGISTRATION");
		doNothing().when(userService).deleteByEmail(any());
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String token = encoder.encode(user.getEmail()+user.getProvider());
		when(userService.findByEmail(any())).thenReturn(user);
		
		MvcResult result = (MvcResult) mvc.perform(get("/unsubscirbe").contentType(MediaType.TEXT_HTML).param("token", token).param("email", user.getEmail()))
				 								.andDo(MockMvcResultHandlers.print())
				 								.andExpect(status().isOk()).andReturn();
		
		verify(userService, times(1)).deleteByEmail(any());
		assertEquals("Uspjesno ste se odjavili s mailing liste!", result.getResponse().getContentAsString());
	}

}
