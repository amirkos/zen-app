package hr.optimus.zen.controller.web;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.common.collect.Lists;

import hr.optimus.zen.config.ZenAppUrlConstants;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.service.UserService;
import hr.optimus.zen.service.impl.MenuServiceImpl;
import hr.optimus.zen.web.model.UserBean;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LoginControllerWebTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Autowired
	private UserService service;
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@MockBean
	MenuServiceImpl menuService;

	@MockBean
	OrderService orderService;

	@Before
	public void setup() {

		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();

		UserBean u = new UserBean();
		u.setEmail("amir.kos@wba.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword(encoder.encode("xxx"));
		u.setProvider("REGISTRATION");
		u.setActive(true);
		u.setAdresses(Lists.newArrayList("Adresa 1", "Adresa 2", "Adresa 3"));
		service.save(u);

		UserBean user1 = new UserBean();
		user1.setEmail("zenkuhinja@gmail.com");
		user1.setFirstName("Iskra");
		user1.setLastName("Radinovic");
		user1.setActive(true);
		user1.setPassword(encoder.encode("xxx"));
		user1.setProvider("ADMIN");
		user1.setAdresses(Lists.newArrayList("Adresa 1", "Adresa 2", "Adresa 3"));
		service.save(user1);
	}

	@Test
	public void testPublicUrl() throws Exception {
		mvc.perform(get("/").contentType(MediaType.TEXT_HTML)).andExpect(status().isOk());
	}

	/**
	 * testing form login with admin privilegies
	 * 
	 * @throws Exception
	 */
	@Test
	public void loginWithAdminAccount() throws Exception {
		mvc.perform(formLogin("/login").user("email", "zenkuhinja@gmail.com").password("password", "xxx"))
				.andExpect(status().is3xxRedirection())
//    	.andExpect(content().contentType(MediaType.TEXT_HTML_VALUE+";charset=UTF-8"))
				.andExpect(model().attributeExists("loggedInUser"))
				.andExpect(view().name(ZenAppUrlConstants.REDIRECT_ADMIN)).andExpect(model().size(1));

	}

	/**
	 * testing login with normal user privilegies
	 * 
	 * @throws Exception
	 */
	@Test
	public void loginWithUserAccount() throws Exception {
		mvc.perform(formLogin("/login").user("email", "amir.kos@wba.com").password("password", "xxx"))
				.andExpect(status().is3xxRedirection())
//    	.andExpect(content().contentType(MediaType.TEXT_HTML_VALUE+ ";charset=UTF-8"))_COOKIE
				.andExpect(model().attributeExists("loggedInUser"))
				.andExpect(view().name(ZenAppUrlConstants.REDIRECT_USER)).andExpect(model().size(1));
	}

	/**
	 * testing login with user privilegies
	 * 
	 * @throws Exception
	 */
	@Test
	public void loginWithNonExistantUser() throws Exception {
		mvc.perform(formLogin("/login").user("email", "amir.kos1@wba.com").password("password", "dxxx"))
				.andExpect(status().isOk())
//    	.andExpect(content().contentType(MediaType.TEXT_HTML_VALUE+";charset=UTF-8"))
				.andExpect(model().attributeExists("error")).andExpect(view().name(ZenAppUrlConstants.INDEX))
				.andExpect(model().size(2));
	}

}
