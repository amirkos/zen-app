package hr.optimus.zen.controller.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.service.OrderService;
import hr.optimus.zen.service.impl.MenuServiceImpl;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AdminControllerWebTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@MockBean
	MenuServiceImpl menuService;

	@MockBean
	OrderService orderService;

	@MockBean
	IngridientTypeWithCaloriesRepository ingridientTypeWithCaloriesRepository;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "ADMIN")
	@Test
	public void testIndex() throws Exception {
		MenuDTO dto = new MenuDTO();
		when(menuService.getDailyMenu(any())).thenReturn(dto);
		ResultActions result = (ResultActions) mvc.perform(get("/admin").contentType(MediaType.TEXT_HTML))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(model().attributeExists("menu"));
	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "ADMIN")
	@Test
	public void testInsertMenu() throws Exception {
		MenuDTO dto = new MenuDTO();
		// kod mocakanja najbolje je za prvi parametar koristiit Mockito.any()
		when(menuService.insertMenu(Mockito.any())).thenReturn(dto);

		mvc.perform(post("/admin/menu").contentType(MediaType.TEXT_HTML)).andDo(MockMvcResultHandlers.print())
				.andExpect(status().is3xxRedirection()).andExpect(model().attributeExists("menu"));
	}

	@Test
	public void testAdminUrlWithNotLoggedUser() throws Exception {
		mvc.perform(get("/admin").contentType(MediaType.TEXT_HTML)).andExpect(status().is3xxRedirection());
	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "REGISTRATION")
	@Test
	public void testAdminUrlWithRegistrationCredentials() throws Exception {
		mvc.perform(get("/admin").contentType(MediaType.TEXT_HTML)).andExpect(status().isForbidden());
	}

	private List<UserOrder> generateUserOrder() {
		Stream<UserOrder> lst = IntStream.range(0, 10).mapToObj(mapper -> {

			UserOrder order = new UserOrder();
			order.setMessage("message:" + mapper);
			order.setQuantity(mapper);
			order.setAddress("adressa" + mapper);
			order.setCellular("cellular" + mapper);
			order.setCreateDateTime(new Timestamp(System.currentTimeMillis()));
			UserBean user = new UserBean();
			user.setEmail("mali_slon@gmail.com" + mapper);
			order.setUser(user);
			return order;
		});

		return lst.collect(Collectors.toList());
	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "ADMIN")
	@Test
	public void testGetDailyOrders() throws Exception {
		List<UserOrder> lst = generateUserOrder();

		when(orderService.getDailyUserOrders(any())).thenReturn(lst);

		MvcResult result = mvc.perform(get("/admin/user/orders").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andReturn();
		verify(orderService, times(1)).getDailyUserOrders(any());
//		assertEquals(ret.getStatusCode(), HttpStatus.OK);
		System.out.println(result.getResponse().getContentAsString());

	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "ADMIN")
	@Test
	public void testGetDailyStatistic() throws Exception {
		MenuCalculationDTO dto = new MenuCalculationDTO();
		
		when(menuService.getDailyTotal(any())).thenReturn(dto);

		MvcResult result = mvc.perform(get("/admin/statistic").contentType(MediaType.TEXT_HTML).param("date", "2011-01-01"))
				.andExpect(status().isOk()).andReturn();
	}

	@WithMockUser(value = "zenkuhinja@gmail.com", password = "xxx", roles = "ADMIN")
	@Test
	public void testGetAllingridients() throws Exception {

		List<IngridientTypeWithCalories> list = new ArrayList<IngridientTypeWithCalories>();

		when(ingridientTypeWithCaloriesRepository.findAll()).thenReturn(list);
		MvcResult result = mvc.perform(get("/admin/ingridients").contentType(MediaType.TEXT_HTML))
				.andExpect(status().isOk()).andReturn();
		verify(ingridientTypeWithCaloriesRepository, times(1)).findAll();
		System.out.println(result.getResponse().getContentAsString());

	}

}
