package hr.optimus.zen.repositories;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import hr.optimus.zen.ZenApp;
import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.UserOrderRepository;
import hr.optimus.zen.repository.UserRepository;
import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZenApp.class)
@DataJpaTest
public class RepositoriesTest {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MenuRepository menuRepository;
	@Autowired
	private UserOrderRepository userOrderRepository;
	@Autowired
	private IngridientTypeWithCaloriesRepository caloriesRepository;

	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;

	
	@Before
	public void contextLoads() {
		createIngridientsTypeWithCalories();
	}

	@Test
	public void testRemoveOrder() {
		Menu menu = menuRepository.getDailyMenu(new Date(System.currentTimeMillis()));
		UserBean u = new UserBean();
		u.setEmail("amir.kos@wba.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword(encoder.encode("xxx"));
		u.setProvider("USER");
		u.setAdresses(Lists.newArrayList("Adresa 1", "Adresa 2", "Adresa 3"));
		userRepository.save(u);

		UserBean userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		UserOrder order = new UserOrder("test", 3, null, "Pužev put 22", "+38599316790", menu, userFromDB,
				new Timestamp(System.currentTimeMillis()), Date.valueOf(LocalDate.now()));
		userFromDB.addOrder(order);

		userRepository.saveAndFlush(userFromDB);

		UserOrder order2 = new UserOrder("test2", 4, null, "Medin put 22", "+38599316789", menu, userFromDB,
				new Timestamp(System.currentTimeMillis()), Date.valueOf("2012-01-11"));
		userFromDB.addOrder(order2);

		userRepository.saveAndFlush(userFromDB);

		UserOrder ordert = new UserOrder();
		ordert.setCreateDate(Date.valueOf(LocalDate.now()));
		ordert.setUser(userFromDB);
		userFromDB = userRepository.findByEmail("amir.kos@wba.com");

		if (userFromDB.getOrders().contains(ordert)) {
			userFromDB.removeOrder(order);
			System.out.println("TRUE");
		}

		userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		assertTrue(userFromDB.getOrders().size() == 1);

	}

	@Test
	public void userTest() {

		List<UserBean> list = new ArrayList<UserBean>();
		UserBean u = new UserBean();
		u.setEmail("amir.kos@wba.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword(encoder.encode("xxx"));
		u.setProvider("USER");

		u.setAdresses(Lists.newArrayList("Adresa 1", "Adresa 2", "Adresa 3"));

		userRepository.save(u);
		list.add(u);

		UserBean u1 = new UserBean();
		u1.setEmail("iskra@wba.com");
		u1.setFirstName("Iskra");
		u1.setLastName("Radinovic");
		u1.setPassword(encoder.encode("xxx"));
		u1.setProvider("ADMIN");

		u1.setAdresses(Lists.newArrayList("Adresa 11", "Adresa 22", "Adresa 33"));

		userRepository.save(u1);

		UserBean user = new UserBean();
		user.setEmail("amir.kos@wba.com");
		if (list.contains(user)) {
			System.out.println("list contains:" + user.getEmail());
		}

		list.add(u1);

		List<UserBean> users = userRepository.findAll();
		assertTrue(users.size() == 2);

		assertTrue("amir.kos@wba.com".equals((userRepository.findByEmail("amir.kos@wba.com")).getEmail()));

		u.getAdresses().remove("Adresa 1");
		userRepository.save(u);

		u = userRepository.findByEmail("amir.kos@wba.com");
		assertTrue(u.getAdresses().size() == 2);

		u.getAdresses().add("Adressa dodana");
		userRepository.save(u);

		u = userRepository.findByEmail("amir.kos@wba.com");
		assertTrue(u.getAdresses().size() == 3);

		userRepository.delete("iskra@wba.com");
		assertTrue(null == userRepository.findByEmail("iskra@wba.com"));

	}
	
	public void createIngridientsTypeWithCalories() {

		for (int i = 1; i <= 100; ++i) {
			IngridientTypeWithCalories cal = new IngridientTypeWithCalories();
			cal.setId(new Long(i));
			cal.setCalories(100 + i);
			cal.setProteins(100 + i);
			cal.setCarbon(100 + i);
			cal.setFat(100 + i);
			cal.setType("MESO" + i);
			caloriesRepository.save(cal);
		}

	}

	private List<Ingridient> addAll(int num) {
		List<Ingridient> ret = new ArrayList<Ingridient>();
		for (int i = 1; i <= num; ++i) {
			Ingridient ingridient = new Ingridient(new BigDecimal(i), new BigDecimal(i));
			ingridient.getCalories().setId(caloriesRepository.findOne(new Long(i)).getId());
			ret.add(ingridient);
		}
		return ret;

	}

	@Test
	public void createMenusAndAddRemoveIngridients() {

		
		Date today = new Date(System.currentTimeMillis());
		Menu m = new Menu();
		m.setMenuDate(Date.valueOf("2011-01-02"));
		m.setAvailable(new Integer(20));
		m.setMenuDescription("Tikvice na naglo, povrtna riza, i gulas od king konga");
		m.setRecordTime(new Timestamp(System.currentTimeMillis()));
		m.setIngridients(addAll(10));
				

		menuRepository.save(m);

		Menu mm = new Menu();
		mm.setMenuDate(today);
		mm.setAvailable(new Integer(20));
		mm.setMenuDescription("Ragu od bivola na naglo, mungo grah riza, i KURAC");
		mm.setRecordTime(new Timestamp(System.currentTimeMillis()));
		mm.setIngridients(addAll(5));// .stream().collect(Collectors.toSet()));

		menuRepository.save(mm);
		System.out.println(
				"DATUMI:" + menuRepository.getDailyMenu(new Date(System.currentTimeMillis())).getMenuDate().getTime()
						+ "-->" + today.getTime());

		assertTrue(menuRepository.getDailyMenu(new Date(System.currentTimeMillis())).getMenuDate().toString()
				.equals(today.toString()));
		
		assertTrue(menuRepository.getDailyMenu(new Date(System.currentTimeMillis())).getIngridients().size() == 5);
		
		mm.getIngridients().addAll(addAll(1));
		menuRepository.save(mm);

		assertTrue(menuRepository.getDailyMenu(new Date(System.currentTimeMillis())).getIngridients().size() == 6);

		mm.getIngridients().remove(0);
//		menuRepository.saveAndFlush(mm);

		mm = menuRepository.getDailyMenu(new Date(System.currentTimeMillis()));

		assertTrue(mm.getIngridients().size() == 5);
	}

	@Test
	public void createAndRemoveUserOrders() {

		Menu menu = menuRepository.getDailyMenu(new Date(System.currentTimeMillis()));
		UserBean u = new UserBean();
		u.setEmail("amir.kos@wba.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword(encoder.encode("xxx"));
		u.setProvider("USER");
		u.setAdresses(Lists.newArrayList("Adresa 1", "Adresa 2", "Adresa 3"));
		userRepository.save(u);

		UserBean userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		UserOrder order = new UserOrder("test", 3, null, "Pužev put 22", "+38599316790", menu, userFromDB,
				new Timestamp(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		userFromDB.addOrder(order);

		userRepository.saveAndFlush(userFromDB);

		UserOrder order2 = new UserOrder("test2", 4, null, "Medin put 22", "+38599316789", menu, userFromDB,
				new Timestamp(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
		userFromDB.addOrder(order2);

		userRepository.saveAndFlush(userFromDB);

		userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		assertTrue(userFromDB.getOrders().size() == 2);

		userRepository.findByEmail("amir.kos@wba.com").getOrders().stream().forEach(m -> {
			System.out.println("#####added:" + m.toString());
		});

		userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		if (userFromDB.getOrders().contains(order)) {
			userFromDB.removeOrder(order);
		}

		if (userFromDB.getOrders().contains(order2)) {
			userFromDB.removeOrder(order2);
		}

		// no need to flush changes hibernate will do that automaticaly

		System.out.println("after deleition");
		userFromDB = userRepository.findByEmail("amir.kos@wba.com");
		assertTrue(userFromDB.getOrders().size() == 0);

		userRepository.findByEmail("amir.kos@wba.com").getOrders().stream().forEach(m -> {
			System.out.println(m.toString());
		});

		System.out.println("after deleition see in db");
		userOrderRepository.findAll().stream().forEach(m -> m.toString());

	}

}
