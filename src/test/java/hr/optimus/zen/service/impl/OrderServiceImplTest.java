package hr.optimus.zen.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.UserOrderRepositoryCustom;
import hr.optimus.zen.repository.UserRepositoryCustom;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.MenuIngridientsDTO;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
	@Mock
	private EmailService emailService;

	@Mock
	private IngridientTypeWithCaloriesRepository ingridientsTypeRepository;

	@Mock
	private MenuRepository menuRepository;

	@Mock
	private UserOrderRepositoryCustom userOrderRepository;
   
	@Mock
	private UserRepositoryCustom userRepositroy;
	
	@InjectMocks
	private OrderServiceImpl orderServiceImpl;

	@Test
	public void testCancelOrder() throws Exception {
		
		Date today = Date.valueOf(LocalDate.now().toString());
		UserBean u = new UserBean();
		u.setEmail("amir.kos@webringapps.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword("xxx");
		u.setProvider("REGISTRATION");

		u.setAdresses(Lists.newArrayList("Vojnoviceva 26").stream().collect(Collectors.toList()));
		u.setUserCellular(Lists.newArrayList("+385993173689").stream().collect(Collectors.toList()));
		
		UserOrder order = new UserOrder();
		order.setId(10l);
		order.setCreateDate(today);
		order.setQuantity(new Integer(1));
		u.addOrder(order);
		
		UserOrder order2 = new UserOrder();
		order2.setId(11l);
		order2.setCreateDate(today);
		order2.setQuantity(new Integer(2));
		u.addOrder(order2);
		
		Menu todayMenu = new Menu();
		todayMenu.setMenuDate(new Date(System.currentTimeMillis()));
		todayMenu.setAvailable(new Integer(20));
		todayMenu.setTotalAvailable(new Integer(20));
		todayMenu.setPrice(new Integer(40));
		
		todayMenu.setMenuDescription("Tikvice na naglo, povrtna riza, i gulas od king konga");
		todayMenu.setRecordTime(new Timestamp(Date.valueOf("2017-01-02").getTime()));
		
		when(userRepositroy.save(u)).thenReturn(u);
		when(userOrderRepository.getDailyOrderIfAny(u.getEmail())).thenReturn(u.getOrders());

		when(menuRepository.getDailyMenu(any())).thenReturn(todayMenu);
		
		doNothing().when(emailService).sendCancleOrderEmail(any(), any());
		
		UserOrder ret = orderServiceImpl.cancelOrder(u);
		assertEquals(ret.getUser()==null, order.getUser()==null);

		assertEquals(u.getOrders().size(), 1);
		
		verify(userRepositroy, times(1)).save(u);
		verify(menuRepository, times(1)).getDailyMenu(any());
		verify(emailService, times(1)).sendCancleOrderEmail(any(), any());
		
	}
	
	private List<Ingridient> createMenuIngridients() {

		List<Ingridient> ret = new ArrayList<Ingridient>();
		for (int i = 1; i <= 100; ++i) {
			Ingridient dto = new Ingridient();
			dto.setPrice(new BigDecimal(i));
			dto.setQuantity(new BigDecimal(1000+i));
			
			IngridientTypeWithCalories calories = new IngridientTypeWithCalories();
			calories.setCalories(new Integer(i));
			calories.setCarbon(new Integer(i));
			calories.setFat(new Integer(i));
			calories.setProteins(new Integer(i));
			calories.setType("Povrce->"+ i);
			dto.setCalories(calories);
			
			ret.add(dto);
		}
		return ret;
	}

	@Test
	public void testOrderMenuNotEnoughOrders() throws Exception {
		UserBean user = new UserBean();
		user.setEmail("amir.kos@webringapps.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		user.setPassword("xxx");
		user.setProvider("REGISTRATION");
		
		UserOrder order = new UserOrder();
		order.setQuantity(new Integer(50));
		order.setAddress("adressa 2");
		order.setCellular("user cellular");
		
		
		Menu daily = new Menu();
		daily.setMenuDescription("desc 12");
		daily.setAvailable(new Integer(20));
		daily.setPrice(new Integer(40));
		daily.setIngridients(createMenuIngridients());
		
		when(menuRepository.getDailyMenu(any())).thenReturn(daily);
		
		UserOrder user_order = orderServiceImpl.orderMenu(order,user );
		
		assertEquals(new Integer(-1), user_order.getQuantity());
		assertEquals("Nemamo dovoljno raspolo�ivih obroka, probajte smanjiti koli?inu", user_order.getMessage());
	}
	
	/**
	 * test ako je korisnik vec ima istu narudđbu.. refreš na browseru..
	 * @throws Exception
	 */
	@Test
	public void testOrderMenuWebPageRefresh() throws Exception {
		Menu daily = new Menu();
		daily.setMenuDescription("desc 12");
		daily.setAvailable(new Integer(20));
		daily.setPrice(new Integer(40));
		daily.setIngridients(createMenuIngridients());
		
		UserBean user = new UserBean();
		user.setEmail("amir.kos@webringapps.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		user.setPassword("xxx");
		user.setProvider("REGISTRATION");
		
		UserOrder order = new UserOrder();
		order.setQuantity(new Integer(5));
		order.setAddress("adressa 2");
		order.setCellular("user cellular");
		order.setCreateDate(Date.valueOf(LocalDate.now()));
		order.setMenu(daily);
		user.addOrder(order);
		
		when(menuRepository.getDailyMenu(any())).thenReturn(daily);
		
		UserOrder user_order = orderServiceImpl.orderMenu(order,user );
		
		verify(userRepositroy, times(0)).save(user);
		verify(menuRepository, times(0)).save(daily);
		
		assertSame(order.getAddress(), user_order.getAddress());
		assertSame(order.getQuantity(), user_order.getQuantity());
		assertSame(order.getMessage(), user_order.getMessage());
		assertSame(order.getQuality(), user_order.getQuality());
		
		assertSame(order.getMenu(), user_order.getMenu());
	}
	
	/**
	 * best case scenario
	 * @throws Exception
	 */
	@Test
	public void testOrderMenu() throws Exception {
		Menu daily = new Menu();
		daily.setMenuDescription("desc 12");
		daily.setAvailable(new Integer(20));
		daily.setPrice(new Integer(40));
		daily.setIngridients(createMenuIngridients());
		
		UserBean user = new UserBean();
		user.setEmail("amir.kos@webringapps.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		user.setPassword("xxx");
		user.setProvider("REGISTRATION");
		
		UserOrder order = new UserOrder();
		order.setQuantity(new Integer(5));
		order.setAddress("adressa 2");
		order.setMenu(daily);
		order.setCellular("user cellular");
	
		
		when(menuRepository.getDailyMenu(any())).thenReturn(daily);
		doNothing().when(emailService).sendOrderConfirmationEmail(any());
		
		UserOrder user_order = orderServiceImpl.orderMenu(order,user );
		
		verify(userRepositroy, times(1)).save(user);
		verify(menuRepository, times(1)).save(daily);
		verify(emailService, times(1)).sendOrderConfirmationEmail(any());
	
		assertSame(order.getAddress(), user_order.getAddress());
		assertSame(order.getQuantity(), user_order.getQuantity());
		assertSame(order.getMessage(), user_order.getMessage());
		assertSame(order.getQuality(), user_order.getQuality());
		
		// novo raspolozivo stanje je manje za onoliko koliko je naruceno
		assertSame((order.getMenu().getAvailable()), user_order.getMenu().getAvailable());
	}
	

	@Test
	public void testGetDailyMenuNotPublishedYet() throws Exception {
		when(menuRepository.getDailyMenu(any())).thenReturn(null);
		Menu menu = orderServiceImpl.getDailyMenu();
		assertEquals("Nothing so far, please come back later!", menu.getMenuDescription());
	}
	@Test
	public void testGetDailyMenu() throws Exception {
		Menu daily = new Menu();
		daily.setMenuDescription("desc 12");
		daily.setAvailable(new Integer(20));
		daily.setPrice(new Integer(40));
		daily.setIngridients(createMenuIngridients());
		when(menuRepository.getDailyMenu(any())).thenReturn(daily);
		Menu menu = orderServiceImpl.getDailyMenu();
		assertEquals(daily, menu);
	}


	@Test
	public void testFindUserOrder() throws Exception {
		UserBean user = new UserBean();
		user.setEmail("amir.kos@webringapps.com");
		user.setFirstName("Amir");
		user.setLastName("Kos");
		user.setPassword("xxx");
		user.setProvider("REGISTRATION");
		
		
		UserOrder order = new UserOrder();
		order.setQuantity(new Integer(5));
		order.setAddress("adressa 2");
		order.setCellular("user cellular");
		
		
		user.addOrder(order);
		
		when(userOrderRepository.getDailyOrderIfAny(user.getEmail())).thenReturn(user.getOrders());
		
		UserOrder ret = orderServiceImpl.findUserOrder(user.getEmail());
		
		assertSame(order, ret);
		
	}

	@Test
	public void testGetDailyUserOrders() throws Exception {
     List<UserOrder> arr = new ArrayList<UserOrder>();
     when(userOrderRepository.getDailyOrders(any())).thenReturn(arr);
     List <UserOrder> ret = orderServiceImpl.getDailyUserOrders(any());
     verify(userOrderRepository, times(1)).getDailyOrders(any());
	}
	
	

	@Test
	public void testGetMenuNutrition() throws Exception {
		Menu daily = new Menu();
		daily.setMenuDescription("desc 12");
		daily.setAvailable(new Integer(20));
		daily.setTotalAvailable(new Integer(40));
		daily.setPrice(new Integer(40));
		daily.setIngridients(createMenuIngridients());
		
		when(menuRepository.getDailyMenu(any())).thenReturn(daily);
		
		MenuIngridientsDTO dto = orderServiceImpl.getMenuNutrition();
		
		dto.getIngridients().forEach(mapper -> System.out.println(mapper.toString()));
		
		
	}

}
