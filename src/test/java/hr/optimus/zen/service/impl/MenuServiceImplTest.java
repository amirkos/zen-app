package hr.optimus.zen.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

import hr.optimus.zen.repository.IngridientTypeWithCaloriesRepository;
import hr.optimus.zen.repository.MenuRepository;
import hr.optimus.zen.repository.impl.UserOrderRepositoryCustomImpl;
import hr.optimus.zen.repository.impl.UserRepositoryCustomImpl;
import hr.optimus.zen.service.EmailService;
import hr.optimus.zen.web.model.Ingridient;
import hr.optimus.zen.web.model.IngridientTypeWithCalories;
import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.UserBean;
import hr.optimus.zen.web.model.UserOrder;
import hr.optimus.zen.web.model.dto.IngridientDTO;
import hr.optimus.zen.web.model.dto.MenuCalculationDTO;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.mapper.IngridientMapper;
import hr.optimus.zen.web.model.dto.mapper.MenuMapper;

@RunWith(MockitoJUnitRunner.class)
public class MenuServiceImplTest {
	@Mock
	private IngridientTypeWithCaloriesRepository ingridientTyperepository;

	@Mock
	private MenuRepository menuRepository;

	@Mock
	private UserOrderRepositoryCustomImpl userOrder;

	@Mock
	private IngridientMapper IngridientMapperImp;
	
	@Mock
	private MenuMapper mapper;
	
	@Mock
	private EmailService service;
	
	@Mock
	private UserRepositoryCustomImpl userRepository;


	@InjectMocks
	private MenuServiceImpl menuServiceImpl;
	
	private final BigDecimal THOUSAND_GRAMS = new BigDecimal(1000);
    private final BigDecimal DELIVERY_BOY = new BigDecimal(100);
    private final BigDecimal FOOD_BOX = BigDecimal.ONE;

	@Before
	public void setUp() throws Exception {
		
	}
	
	private List<UserBean> createUsers() {
		
		List<UserBean> lst = new ArrayList<UserBean>();
		UserBean u = new UserBean();
		u.setEmail("amir.kos@webringapps.com");
		u.setFirstName("Amir");
		u.setLastName("Kos");
		u.setPassword("xxx");
		u.setProvider("REGISTRATION");

		u.setAdresses(Lists.newArrayList("Vojnoviceva 26").stream().collect(Collectors.toList()));
		u.setUserCellular(Lists.newArrayList("+385993173689").stream().collect(Collectors.toList()));

		userRepository.save(u);

		
		UserBean u2 = new UserBean();
		u2.setEmail("amir.kos@gmail.com");
		u2.setFirstName("Amir");
		u2.setLastName("Kos");
		u2.setPassword("yyy");
		u2.setProvider("GOOGLE");
		u2.setAdresses(
				Lists.newArrayList("Adresa 777").stream().collect(Collectors.toList()));
		u2.setUserCellular(Lists.newArrayList("+385993176666").stream().collect(Collectors.toList()));
		lst.add(u);
		lst.add(u2);
		return lst;

	}
	@Test
	public void insertMenuAndSendEmails()
	{
		MenuDTO dto = new MenuDTO();
		dto.setSendEmail(true);
		dto.setMenuDescription("mali slon");
		dto.setIngridients(new ArrayList<IngridientDTO>());
		dto.setAvailable(new Integer(30));
		dto.setTotalAvailable(new Integer(30));
		
		Menu dailMenu = new Menu();
		dailMenu.setAvailable(new Integer(30));
		dailMenu.setTotalAvailable(new Integer(30));
		dailMenu.setIngridients(new ArrayList<Ingridient>());
		
		List<UserBean> users = createUsers();
		
		when(menuRepository.getDailyMenu(any())).thenReturn(dailMenu);
		when(menuRepository.save(dailMenu)).thenReturn(dailMenu);
		when(mapper.toEntity(dto)).thenReturn(dailMenu);
		when(mapper.toDTO(dailMenu)).thenReturn(dto);
		when(userRepository.findAllUsers()).thenReturn(users);
		doNothing().when(service).sendMenuEmail(any(), any());
		
		MenuDTO ret = menuServiceImpl.insertMenu(dto);
		
		assertThat(dto).isSameAs(ret);
        
		verify(mapper, times(1)).toDTO(dailMenu);
		verify(mapper, times(1)).toEntity(dto);
		
		verify(menuRepository, times(1)).save(dailMenu);
		verify(service, times(users.size())).sendMenuEmail(any(), any());
		assertEquals(dto.getMenuDescription(), ret.getMenuDescription());
		

		assertEquals(dto, ret);
	}
	
	@Test
	public void insertMenu()
	{
		MenuDTO dto = new MenuDTO();
		dto.setSendEmail(false);
		dto.setMenuDescription("mali slon");
		dto.setIngridients(new ArrayList<IngridientDTO>());
		dto.setAvailable(new Integer(30));
		dto.setTotalAvailable(new Integer(30));
		dto.setPrice(new Integer(40));
		
		Menu dailMenu = new Menu();
		dailMenu.setAvailable(new Integer(30));
		dailMenu.setTotalAvailable(new Integer(30));
		
		dailMenu.setIngridients(new ArrayList<Ingridient>());
		
		when(menuRepository.getDailyMenu(any())).thenReturn(null);
		when(menuRepository.save(dailMenu)).thenReturn(dailMenu);
		when(mapper.toEntity(dto)).thenReturn(dailMenu);
		when(mapper.toDTO(dailMenu)).thenReturn(dto);
		
		MenuDTO ret = menuServiceImpl.insertMenu(dto);
		
		assertThat(dto).isSameAs(ret);
        
		verify(mapper, times(1)).toDTO(dailMenu);
		verify(mapper, times(1)).toEntity(dto);
		
		verify(menuRepository, times(1)).save(dailMenu);
		verify(service, times(0)).sendMenuEmail(any(), any());
		assertEquals(dto.getMenuDescription(), ret.getMenuDescription());

		assertEquals(dto, ret);
		
	}
	
	@Test
	public void updateMenu()
	{
		MenuDTO dto = new MenuDTO();
		dto.setSendEmail(false);
		dto.setMenuDescription("mali slon");
		dto.setIngridients(new ArrayList<IngridientDTO>());
		dto.setAvailable(new Integer(30));
		dto.setTotalAvailable(new Integer(30));
		
		Menu dailMenu = new Menu();
		dailMenu.setAvailable(new Integer(30));
		dailMenu.setTotalAvailable(new Integer(30));
		dailMenu.setIngridients(new ArrayList<Ingridient>());
		
		when(menuRepository.getDailyMenu(any())).thenReturn(dailMenu);
		when(menuRepository.save(dailMenu)).thenReturn(dailMenu);
		when(mapper.toEntity(dto)).thenReturn(dailMenu);
		when(mapper.toDTO(dailMenu)).thenReturn(dto);
		
		MenuDTO ret = menuServiceImpl.insertMenu(dto);
		
		assertThat(dto).isSameAs(ret);
        
		verify(mapper, times(1)).toDTO(dailMenu);
		verify(mapper, times(1)).toEntity(dto);
		
		verify(menuRepository, times(1)).save(dailMenu);
		verify(service, times(0)).sendMenuEmail(any(), any());
		assertEquals(dto.getMenuDescription(), ret.getMenuDescription());
		assertEquals(dto, ret);
	}
	
	@Test
	/**
	 * slucaj kad se smanjuje broj menija.. mora ostati 0 menija.. u tom slucaju se 
	 * se unosi available kao negativan broj
	 */
	public void updateZeroMenuAvailability()
	{
		// dto from frontedn
		MenuDTO updateMenuDTO = new MenuDTO();
		updateMenuDTO.setSendEmail(false);
		updateMenuDTO.setMenuDescription("promjena descritpitona");
		updateMenuDTO.setMenuFacts("some new menu facts");
		updateMenuDTO.setIngridients(new ArrayList<IngridientDTO>());
		updateMenuDTO.setAvailable(new Integer(-3));
		
		// entity from upper dto
		Menu updateMenu = new Menu();
		updateMenu.setMenuDescription("promjena descritpitona");
		updateMenu.setMenuFacts("some new menu facts");
		updateMenu.setIngridients(new ArrayList<Ingridient>());
		updateMenu.setAvailable(new Integer(-3));
		
		// existing daily menu from DB
		Menu menuFromDB = new Menu();
		menuFromDB.setMenuDescription("old description");
		menuFromDB.setMenuFacts("old menu facts");
		menuFromDB.setAvailable(new Integer(3));
		menuFromDB.setTotalAvailable(new Integer(3));
		menuFromDB.setIngridients(new ArrayList<Ingridient>());
		
		Menu expectedMenuValueAfterUpdate = new Menu();
		expectedMenuValueAfterUpdate.setAvailable(menuFromDB.getAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdate.setTotalAvailable(menuFromDB.getTotalAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdate.setMenuDescription("promjena descritpitona");
		expectedMenuValueAfterUpdate.setMenuFacts("some new menu facts");
		expectedMenuValueAfterUpdate.setIngridients(new ArrayList<Ingridient>());
		
		MenuDTO expectedMenuValueAfterUpdateDTO = new MenuDTO();
		expectedMenuValueAfterUpdateDTO.setAvailable(menuFromDB.getAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdateDTO.setTotalAvailable(menuFromDB.getTotalAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdateDTO.setMenuDescription("promjena descritpitona");
		expectedMenuValueAfterUpdateDTO.setMenuFacts("some new menu facts");
		expectedMenuValueAfterUpdateDTO.setIngridients(new ArrayList<IngridientDTO>());
		
		// convert DTO to entity
		when(mapper.toEntity(updateMenuDTO)).thenReturn(updateMenu);
		// get menu from DB
		when(menuRepository.getDailyMenu(any())).thenReturn(menuFromDB);
		
		// save updated menu 
		when(menuRepository.save(menuFromDB)).thenReturn(expectedMenuValueAfterUpdate);
		
		// return updated menu to dto for return to front end
		when(mapper.toDTO(expectedMenuValueAfterUpdate)).thenReturn(expectedMenuValueAfterUpdateDTO);
		
		MenuDTO ret = menuServiceImpl.insertMenu(updateMenuDTO);
		
		// input DTO is same as output DTO i.e. menu was really updated
		assertThat(expectedMenuValueAfterUpdateDTO).isSameAs(ret);
		
		// to entity on input dto
		verify(mapper, times(1)).toEntity(updateMenuDTO);
        
		// to dto on return
		verify(mapper, times(1)).toDTO(expectedMenuValueAfterUpdate);
		
		
		verify(menuRepository, times(1)).save(menuFromDB);
		
		verify(service, times(0)).sendMenuEmail(any(), any());
		
		assertEquals(updateMenuDTO.getMenuDescription(), ret.getMenuDescription());
		assertEquals(updateMenuDTO.getMenuFacts(), ret.getMenuFacts());
		
		assertTrue(0 == ret.getAvailable());
		
		assertTrue(0 == ret.getTotalAvailable());
		
		assertEquals(expectedMenuValueAfterUpdateDTO, ret);
	}
	
	
	public void updateDecreaseMenuAvailability()
	{
		// dto from frontedn
		MenuDTO updateMenuDTO = new MenuDTO();
		updateMenuDTO.setSendEmail(false);
		updateMenuDTO.setMenuDescription("promjena descritpitona");
		updateMenuDTO.setMenuFacts("some new menu facts");
		updateMenuDTO.setIngridients(new ArrayList<IngridientDTO>());
		updateMenuDTO.setAvailable(new Integer(-3));
		
		// entity from upper dto
		Menu updateMenu = new Menu();
		updateMenu.setMenuDescription("promjena descritpitona");
		updateMenu.setMenuFacts("some new menu facts");
		updateMenu.setIngridients(new ArrayList<Ingridient>());
		updateMenu.setAvailable(new Integer(-3));
		
		// existing daily menu from DB
		Menu menuFromDB = new Menu();
		menuFromDB.setMenuDescription("old description");
		menuFromDB.setMenuFacts("old menu facts");
		menuFromDB.setAvailable(new Integer(17));
		menuFromDB.setTotalAvailable(new Integer(30));
		menuFromDB.setIngridients(new ArrayList<Ingridient>());
		
		Menu expectedMenuValueAfterUpdate = new Menu();
		expectedMenuValueAfterUpdate.setAvailable(menuFromDB.getAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdate.setTotalAvailable(menuFromDB.getTotalAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdate.setMenuDescription("promjena descritpitona");
		expectedMenuValueAfterUpdate.setMenuFacts("some new menu facts");
		expectedMenuValueAfterUpdate.setIngridients(new ArrayList<Ingridient>());
		
		MenuDTO expectedMenuValueAfterUpdateDTO = new MenuDTO();
		expectedMenuValueAfterUpdateDTO.setAvailable(menuFromDB.getAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdateDTO.setTotalAvailable(menuFromDB.getTotalAvailable()+updateMenu.getAvailable());
		expectedMenuValueAfterUpdateDTO.setMenuDescription("promjena descritpitona");
		expectedMenuValueAfterUpdateDTO.setMenuFacts("some new menu facts");
		expectedMenuValueAfterUpdateDTO.setIngridients(new ArrayList<IngridientDTO>());
		
		// convert DTO to entity
		when(mapper.toEntity(updateMenuDTO)).thenReturn(updateMenu);
		// get menu from DB
		when(menuRepository.getDailyMenu(any())).thenReturn(menuFromDB);
		
		// save updated menu 
		when(menuRepository.save(menuFromDB)).thenReturn(expectedMenuValueAfterUpdate);
		
		// return updated menu to dto for return to front end
		when(mapper.toDTO(expectedMenuValueAfterUpdate)).thenReturn(expectedMenuValueAfterUpdateDTO);
		
		MenuDTO ret = menuServiceImpl.insertMenu(updateMenuDTO);
		
		// input DTO is same as output DTO i.e. menu was really updated
		assertThat(expectedMenuValueAfterUpdateDTO).isSameAs(ret);
		
		// to entity on input dto
		verify(mapper, times(1)).toEntity(updateMenuDTO);
        
		// to dto on return
		verify(mapper, times(1)).toDTO(expectedMenuValueAfterUpdate);
		
		
		verify(menuRepository, times(1)).save(menuFromDB);
		
		verify(service, times(0)).sendMenuEmail(any(), any());
		
		assertEquals(updateMenuDTO.getMenuDescription(), ret.getMenuDescription());
		assertEquals(updateMenuDTO.getMenuFacts(), ret.getMenuFacts());
		
		assertTrue(14 == ret.getAvailable());
		
		assertTrue(27 == ret.getTotalAvailable());
		
		assertEquals(expectedMenuValueAfterUpdateDTO, ret);
	}
	@Test
	public void updateDecreaseMenuAvilabiltyNotPossible() 
	{
		MenuDTO dto = new MenuDTO();
		dto.setSendEmail(false);
		
		// meni iz baze.. ima 30 menija  preostalih
		Menu fromDb = new Menu();
		fromDb.setAvailable(new Integer(30));
		fromDb.setTotalAvailable(new Integer(30));
		fromDb.setIngridients(new ArrayList<Ingridient>());
		
		// pokusavam staviti novi broj menija koji je zapravo veci od preostalog broja menija..
		// nista se ne bi smjelo azurirati
		Menu fromWeb = new Menu();
		fromWeb.setAvailable(new Integer(-31));
		fromWeb.setIngridients(new ArrayList<Ingridient>());
		
		when(mapper.toEntity(dto)).thenReturn(fromWeb);
		when(menuRepository.getDailyMenu(any())).thenReturn(fromDb);
		
		when(menuRepository.save(fromDb)).thenReturn(fromDb);
		
		MenuDTO retDTO = new MenuDTO();
		retDTO.setAvailable(fromDb.getAvailable());
		retDTO.setTotalAvailable(fromDb.getTotalAvailable());
		
		when(mapper.toDTO(fromDb)).thenReturn(retDTO);
		
		MenuDTO ret = menuServiceImpl.insertMenu(dto);
		
		assertEquals(retDTO, ret);
		
		assertThat(ret.getAvailable()).isSameAs(retDTO.getAvailable());
		assertThat(ret.getTotalAvailable()).isSameAs(retDTO.getTotalAvailable());
        
		verify(mapper, times(1)).toDTO(fromDb);
		verify(mapper, times(1)).toEntity(dto);
		
		verify(menuRepository, times(1)).save(fromDb);
		verify(service, times(0)).sendMenuEmail(any(), any());
		assertEquals(dto.getMenuDescription(), ret.getMenuDescription());
	}
	
	/**
	 * metoda koja pokusava azurirati broj preostalih menija
	 * radi se update na vec postojecem meniju.. stavlja se novi broj preostalih menija
	 */
	
	@Test
	public void updateIncreaseMenuAvilability()
	{
		MenuDTO dto = new MenuDTO();
		dto.setSendEmail(false);
		
		Menu fromDb = new Menu();
		fromDb.setAvailable(new Integer(40));
		fromDb.setTotalAvailable(new Integer(30));
		fromDb.setIngridients(new ArrayList<Ingridient>());
		
		Menu fromWeb = new Menu();
		fromWeb.setAvailable(new Integer(5));
		fromWeb.setIngridients(new ArrayList<Ingridient>());
		
		when(mapper.toEntity(dto)).thenReturn(fromWeb);
		when(menuRepository.getDailyMenu(any())).thenReturn(fromDb);
		
		when(menuRepository.save(fromDb)).thenReturn(fromDb);
		
		MenuDTO retDTO = new MenuDTO();
		retDTO.setAvailable(fromDb.getAvailable()+fromWeb.getAvailable());
		retDTO.setTotalAvailable(fromDb.getTotalAvailable()+fromWeb.getAvailable());
		
		when(mapper.toDTO(fromDb)).thenReturn(retDTO);
		
		MenuDTO ret = menuServiceImpl.insertMenu(dto);
		
		assertEquals(retDTO, ret);
		
		assertThat(ret.getAvailable()).isSameAs(retDTO.getAvailable());
		assertThat(ret.getTotalAvailable()).isSameAs(retDTO.getTotalAvailable());
        
		verify(mapper, times(1)).toDTO(fromDb);
		verify(mapper, times(1)).toEntity(dto);
		
		verify(menuRepository, times(1)).save(fromDb);
		verify(service, times(0)).sendMenuEmail(any(), any());
		assertEquals(dto.getMenuDescription(), ret.getMenuDescription());
		
	}
	@Test
	public void getDailyMenuTest() {

		MenuDTO dailymenu = new MenuDTO();
		dailymenu.setAvailable(10);
		
		Menu entity = new Menu();
		entity.setAvailable(10);

		Date today = new Date(System.currentTimeMillis());

		when(menuRepository.getDailyMenu(today)).thenReturn(entity);
		when(mapper.toDTO(entity)).thenReturn(dailymenu);


		MenuDTO menu = menuServiceImpl.getDailyMenu(today);
		
		assertThat(menu).isSameAs(dailymenu);
         
		verify(mapper, times(1)).toDTO(entity);
		verify(menuRepository, times(1)).getDailyMenu(today);
	
		assertEquals(menu.getAvailable().longValue(), 10L);

		assertEquals(dailymenu, menu);
	}
	
	private List<Ingridient> createMenuIngridients() {

		List<Ingridient> ret = new ArrayList<Ingridient>();
		for (int i = 1; i <= 100; ++i) {
			Ingridient dto = new Ingridient();
			dto.setPrice(new BigDecimal(i));
			dto.setQuantity(new BigDecimal(1000+i));
			
			IngridientTypeWithCalories calories = new IngridientTypeWithCalories();
			calories.setCalories(new Integer(i));
			calories.setCarbon(new Integer(i));
			calories.setFat(new Integer(i));
			calories.setProteins(new Integer(i));
			calories.setType("Povrce->"+ i);
			dto.setCalories(calories);
			
			ret.add(dto);
		}
		return ret;
	}
	
	/**
	 * dnevni total testiranje
	 * @throws Exception
	 */
	@Test
	public void getDailyTotalTest() throws Exception{
		
		Menu daily = new Menu();
		daily.setTotalAvailable(IntStream.range(0, 20).sum());
		daily.setAvailable(daily.getTotalAvailable());
		daily.setPrice(new Integer(45));
		daily.setMenuDate(Date.valueOf(LocalDate.now()));
		daily.setMenuDescription("malo mrkve malo tikvica");
		daily.setIngridients(createMenuIngridients());
		
		List<UserOrder> orders = IntStream.range(0, 20).mapToObj(mapper->{
					UserBean user = new UserBean();
					user.setEmail("email->" + mapper);
					user.getAdresses().add("adress"+ mapper);
					user.getUserCellular().add("cellular"+ mapper);
					daily.setAvailable(daily.getAvailable() - mapper);
					UserOrder order = new UserOrder("meni testi"+ mapper, 
							mapper, 
							null, 
							"adress"+ mapper, 
							"cellular"+ mapper, 
							daily, 
							user, 
							new Timestamp(System.currentTimeMillis()), 
							Date.valueOf(LocalDate.now()));
					return order;
		}).collect(Collectors.toList());
		
		when(userOrder.getDailyOrders(any())).thenReturn(orders);
		
		MenuCalculationDTO dto = menuServiceImpl.getDailyTotal(any());
		
		// generiro sam 20 user-a
		assertEquals(dto.getConsumers().size(), 20);
		// generiro sam 100 namirnica
		assertEquals(dto.getIngridients().size(), 100);
		
		double orders_quatity = orders.stream().mapToDouble(mapper->mapper.getQuantity()).sum();
		double generated_quantity = IntStream.range(0, 20).mapToDouble(mapper->new Double(mapper)).sum();
		assertEquals(orders_quatity,generated_quantity, new Double(0).doubleValue());
		
		// sumira sve generirane namirnice
		double ingridients_sum  = daily.getIngridients().stream()
				.mapToDouble(mapper->{
					double result = mapper.getPrice().doubleValue() * (mapper.getQuantity().doubleValue()/THOUSAND_GRAMS.doubleValue());
					return result;
				}).sum();
		
		System.out.println("total_spent:"+dto.getTotalSpent());
		System.out.println("ingridients_sum:"+ingridients_sum);
		System.out.println(IntStream.range(0, 20).sum());
		//190 komada od 0 do 20 0+1+2+3+4+5+6+7+8+9+10+11+12+13+14...+19
		System.out.println("broj prodanih:"+orders_quatity);
		assertEquals(ingridients_sum,
				(dto.getTotalSpent()
						.subtract(DELIVERY_BOY)
						.subtract(FOOD_BOX.multiply(new BigDecimal(orders_quatity)))
				.setScale(2, RoundingMode.HALF_UP))
				.doubleValue(),
				new Double(0));
		
		// 190 generiranih narudzbi-- 190 potrosenih naruđbi
		assertTrue(daily.getAvailable()==0);
		
	}
	
	@Test
	public void testDeletMenu() {
		
	}

}
