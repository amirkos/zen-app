package hr.optimus.zen.dto;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hr.optimus.zen.web.model.Menu;
import hr.optimus.zen.web.model.dto.MenuDTO;
import hr.optimus.zen.web.model.dto.mapper.MenuMapper;
import hr.optimus.zen.web.model.dto.mapper.impl.MenuMapperImp;

@RunWith(MockitoJUnitRunner.class)
public class MenuMapperImpTest {
	@Mock
	private MenuMapper mapper;
	@InjectMocks
	private MenuMapperImp menuMapperImp;

	@Test
	public void testToEntity() throws Exception {
		MenuDTO dailymenu = new MenuDTO();
		dailymenu.setAvailable(10);
		
		Menu menu = new Menu();
		menu.setAvailable(10);
		
		dailymenu.setAvailable(10);
		when(mapper.toEntity(dailymenu)).thenReturn(menu);
		when(menuMapperImp.toEntity(dailymenu)).thenReturn(menu);
		
		Menu to = menuMapperImp.toEntity(dailymenu);
		
		assertSame(to.getAvailable(), dailymenu.getAvailable());
	}

	@Test
	public void testToDTO() throws Exception {
		
		MenuDTO menuDTO = new MenuDTO();
		menuDTO.setAvailable(10);
		
		
		Menu menu = new Menu();
		menu.setAvailable(10);
		
		menuDTO.setAvailable(10);
		when(mapper.toDTO(menu)).thenReturn(menuDTO);
		when(menuMapperImp.toDTO(menu)).thenReturn(menuDTO);
		
		MenuDTO to = menuMapperImp.toDTO(menu);
		
		assertSame(to.getAvailable(), menu.getAvailable());
	}

}
