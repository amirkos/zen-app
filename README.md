[![CircleCI](https://circleci.com/bb/amirkos/zen-app.svg?style=svg)](https://circleci.com/bb/amirkos/zen-app)

[![codecov](https://codecov.io/bb/amirkos/zen-app/branch/master/graph/badge.svg?token=LjtGlzOtRY)](https://codecov.io/bb/amirkos/zen-app)



# Zen-k-app

usage: mvn install

#run with profile local
starting:  mvn spring-boot:run -Drun.profiles=local

#run with profile dev
starting:  mvn spring-boot:run -Drun.profiles=dev

#if cant be started from ste i.e eclipse then run:
mvn compile
